package com.cobasoft.mad_vendedor;

import android.app.Activity;
import android.app.Application;

import com.cobasoft.mad_vendedor.di.component.DaggerApiComponent;
import com.cobasoft.mad_vendedor.di.module.ApiModule;
import com.cobasoft.mad_vendedor.di.module.DbModule;

import javax.inject.Inject;

import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;

//siempre que se extiende de Application se lo declara con name en el manifest
public class AppController extends Application implements HasActivityInjector {

    @Inject
    DispatchingAndroidInjector<Activity> dispatchingAndroidInjector;

    @Override
    public DispatchingAndroidInjector<Activity> activityInjector() {
        return dispatchingAndroidInjector;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //a partir de la palabrar Dagger va el nombre de la interfaz que tiene los componentes
        //entonces Dagger generará en este caso 'DaggerApiComponent'
        DaggerApiComponent.builder()
                .application(this)
                .apiModule(new ApiModule())
                .dbModule(new DbModule())
                .build()
                .inject(this);
    }
}