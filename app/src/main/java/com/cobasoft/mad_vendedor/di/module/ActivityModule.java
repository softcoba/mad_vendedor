package com.cobasoft.mad_vendedor.di.module;

import com.cobasoft.mad_vendedor.ui.MainActivity;
import com.cobasoft.mad_vendedor.ui.sesion.activity.CrearActualizarVendedorActivity;
import com.cobasoft.mad_vendedor.ui.sesion.activity.LoginActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityModule {

    @ContributesAndroidInjector()
    abstract CrearActualizarVendedorActivity contributeCrearActualizarVendedorActivity();

    @ContributesAndroidInjector()
    abstract LoginActivity contributeLoginActivity();

    @ContributesAndroidInjector()
    abstract MainActivity contributeMainActivity();

}
