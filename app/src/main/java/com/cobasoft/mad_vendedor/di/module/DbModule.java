package com.cobasoft.mad_vendedor.di.module;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.room.Room;

import com.cobasoft.mad_vendedor.data.local.AppDatabase;
import com.cobasoft.mad_vendedor.data.local.dao.SesionDao;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import static com.cobasoft.mad_vendedor.AppConstants.ROOM_DATABASE;

// el modulo se encarga de proveer a nuestra actividad todas las instancias necesarias, para que funcione nuestras clases
// debemos crear los modulos para que dagger sepa como devolver los objetos que serán requeridos
@Module
public class DbModule {
    // Los Provides van delante de los métodos que proporcionan objetos para la inyección de dependencias
    @Provides
    @Singleton
    AppDatabase provideDatabase(@NonNull Application application) {
        return Room.databaseBuilder(application,
                AppDatabase.class, ROOM_DATABASE)
                .allowMainThreadQueries().build();
    }

    @Provides
    @Singleton
    SesionDao provideSesionDao(@NonNull AppDatabase appDatabase) {
        return appDatabase.sesionDao();
    }

}


