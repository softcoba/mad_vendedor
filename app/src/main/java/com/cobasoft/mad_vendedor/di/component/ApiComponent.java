package com.cobasoft.mad_vendedor.di.component;

import android.app.Application;

import com.cobasoft.mad_vendedor.AppController;
import com.cobasoft.mad_vendedor.di.module.ActivityModule;
import com.cobasoft.mad_vendedor.di.module.ApiModule;
import com.cobasoft.mad_vendedor.di.module.DbModule;
import com.cobasoft.mad_vendedor.di.module.FragmentModule;
import com.cobasoft.mad_vendedor.di.module.ViewModelModule;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.support.AndroidSupportInjectionModule;

@Singleton
// el componente es el puente entre los módulos creados y la parte del código que va a solicitar
// esos objetos para hacer la inyección de dependencias
@Component(modules = {
        ActivityModule.class,
        ApiModule.class,
        DbModule.class,
        FragmentModule.class,
        ViewModelModule.class,
        AndroidSupportInjectionModule.class})
public interface ApiComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        @BindsInstance
        Builder apiModule(ApiModule apiModule);

        @BindsInstance
        Builder dbModule(DbModule dbModule);

        ApiComponent build();
    }

    void inject(AppController appController);
}
