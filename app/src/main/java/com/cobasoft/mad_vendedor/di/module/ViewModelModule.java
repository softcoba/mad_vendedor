package com.cobasoft.mad_vendedor.di.module;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.cobasoft.mad_vendedor.factory.ViewModelFactory;
import com.cobasoft.mad_vendedor.ui.sesion.viewmodel.CrearActualizarVendedorViewModel;
import com.cobasoft.mad_vendedor.ui.sesion.viewmodel.LoginViewModel;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module
public abstract class ViewModelModule {

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);

    @Binds
    @IntoMap
    @ViewModelKey(CrearActualizarVendedorViewModel.class)
    protected abstract ViewModel crearActualizarVendedorViewModel(CrearActualizarVendedorViewModel crearActualizarVendedorViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel.class)
    protected abstract ViewModel loginViewModel(LoginViewModel loginViewModel);

}
