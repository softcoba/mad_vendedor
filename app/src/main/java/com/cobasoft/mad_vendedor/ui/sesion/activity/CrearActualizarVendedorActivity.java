package com.cobasoft.mad_vendedor.ui.sesion.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;

import androidx.lifecycle.ViewModelProviders;

import com.cobasoft.mad_vendedor.R;
import com.cobasoft.mad_vendedor.data.local.entity.RequestEntity;
import com.cobasoft.mad_vendedor.data.remote.model.Vendedor;
import com.cobasoft.mad_vendedor.factory.ViewModelFactory;
import com.cobasoft.mad_vendedor.ui.base.BaseActivity;
import com.cobasoft.mad_vendedor.ui.sesion.viewmodel.CrearActualizarVendedorViewModel;
import com.shashank.sony.fancytoastlib.FancyToast;

import javax.inject.Inject;

import dagger.android.AndroidInjection;


public class CrearActualizarVendedorActivity extends BaseActivity {

    // UI references.
    private AutoCompleteTextView mCorreoActxv;
    private EditText mNombreEdt, mApellidoEdt, mCelularEdt, mConvencionalEdt, mDireccionEdt, mPasswordPedtx;
    private Button mRegistrarseBtn;
    private View mProgressView;
    private View mLoginFormView;

    @Inject
    ViewModelFactory viewModelFactory;

    CrearActualizarVendedorViewModel crearActualizarVendedorViewModel;

    private void dataInjection() {
        AndroidInjection.inject(this);
        crearActualizarVendedorViewModel = ViewModelProviders.of(this, viewModelFactory).get(CrearActualizarVendedorViewModel.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_cuenta);

        dataInjection();
        setViews();

        mRegistrarseBtn.setOnClickListener(v -> {

            RequestEntity requestEntity = crearRequestEntity();

            if (!validarFormularioView()) {

                crearActualizarVendedorViewModel.crearVendedor(requestEntity);
                crearActualizarVendedorViewModel.obtenerVendedor().observe(this, resource -> {
                    if (resource.isLoading()) {
                        // Show a progress spinner, and kick off a background task to
                        // perform the user register attempt.
                        showProgress(true);
                    } else if (resource.data != null) {

                        if (resource.data.getEstado() == PETICION_CORRECTA)
                            finish();

                        FancyToast.makeText(this, resource.data.getVendedor().getNombre(),
                                FancyToast.LENGTH_LONG, FancyToast.SUCCESS, true).show();

                    }
                    showProgress(false);
                });
            }
        });

        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
    }

    private void setViews() {
        mNombreEdt = findViewById(R.id.edt_nombre);
        mApellidoEdt = findViewById(R.id.edt_apellido);
        mCelularEdt = findViewById(R.id.edt_celular);
        mConvencionalEdt = findViewById(R.id.edt_convencional);
        mCorreoActxv = findViewById(R.id.actxv_correo);
        mDireccionEdt = findViewById(R.id.edt_direccion);
        mPasswordPedtx = findViewById(R.id.pedtx_password);
        mRegistrarseBtn = findViewById(R.id.registrarse);
    }

    private RequestEntity crearRequestEntity() {

        RequestEntity requestEntity = new RequestEntity();
        Vendedor vendedor = new Vendedor();
        // Store values at the time of the login attempt.
        String nombre = mNombreEdt.getText().toString();
        String apellido = mApellidoEdt.getText().toString();
        String celular = mCelularEdt.getText().toString();
        String correo = mCorreoActxv.getText().toString();
        String password = mPasswordPedtx.getText().toString();

        vendedor.setNombre(nombre);
        vendedor.setApellido(apellido);
        vendedor.setCelular(celular);
        vendedor.setCorreo(correo);
        vendedor.setPassword(password);

        requestEntity.setVendedor(vendedor);

        return requestEntity;
    }

    private boolean validarFormularioView() {

        // Reset errors.
        mCorreoActxv.setError(null);
        mPasswordPedtx.setError(null);

//        String correo = requestEntity.getVendedor().getCorreo();
//        String password = requestEntity.getVendedor().getPassword();
        // Store values at the time of the login attempt.
        String correo = mCorreoActxv.getText().toString();
        String password = mPasswordPedtx.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordPedtx.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordPedtx;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(correo)) {
            mCorreoActxv.setError(getString(R.string.error_field_required));
            focusView = mCorreoActxv;
            cancel = true;
        } else if (!isEmailValid(correo)) {
            mCorreoActxv.setError(getString(R.string.error_invalid_email));
            focusView = mCorreoActxv;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        }
        return cancel;
    }

    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    private void showProgress(final boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    private void launch() {


    }

}

