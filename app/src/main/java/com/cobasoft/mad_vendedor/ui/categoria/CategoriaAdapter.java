package com.cobasoft.mad_vendedor.ui.categoria;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.cobasoft.mad_vendedor.R;
import com.google.common.collect.BiMap;

import java.util.List;
import java.util.Map;


public class CategoriaAdapter extends ArrayAdapter<String>{// aqui debe ir esto <BeanCategoria> {

    private static final String TAG = CategoriaAdapter.class.getSimpleName();

    private List<String> mLstCategorias;
    private BiMap<String, String> biMap_uuid_descripcion_Referencia;
    private Map<String, Boolean> map_uuid_estado;

    private Context mContext;

    public CategoriaAdapter(Context context, int resourceId, BiMap<String, String> hash_map_checks,
                            List<String> mLstCategorias, Map<String, Boolean> map_uuid_estado) {
        super(context, resourceId, mLstCategorias);

        this.mContext = context;
        this.mLstCategorias = mLstCategorias;
        this.biMap_uuid_descripcion_Referencia = hash_map_checks;

        this.map_uuid_estado = map_uuid_estado;
    }

    @Override
    public int getCount() {
        return super.getCount();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = inflater.inflate(R.layout.item_categoria, null);
        TextView txvCategoria = convertView.findViewById(R.id.txv_categoria);
        ImageView imgCategoria = convertView.findViewById(R.id.img_categoria);

        txvCategoria.setText(mLstCategorias.get(position));

//        Picasso.get()
//                .load(imageUrls[position])
//                .fit()
//                .into((ImageView) convertView);


        return convertView;
    }

}