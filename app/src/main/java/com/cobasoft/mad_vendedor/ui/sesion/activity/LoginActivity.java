package com.cobasoft.mad_vendedor.ui.sesion.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.lifecycle.ViewModelProviders;

import com.cobasoft.mad_vendedor.R;
import com.cobasoft.mad_vendedor.data.local.entity.RequestEntity;
import com.cobasoft.mad_vendedor.data.remote.model.Vendedor;
import com.cobasoft.mad_vendedor.factory.ViewModelFactory;
import com.cobasoft.mad_vendedor.ui.MainActivity;
import com.cobasoft.mad_vendedor.ui.base.BaseActivity;
import com.cobasoft.mad_vendedor.ui.sesion.viewmodel.LoginViewModel;
import com.shashank.sony.fancytoastlib.FancyToast;

import javax.inject.Inject;

import dagger.android.AndroidInjection;


public class LoginActivity extends BaseActivity {

    // UI references.
    private AutoCompleteTextView mCorreoActxv;
    private EditText mPasswordPedtx;
    private View mProgressView;
    private View mLoginFormView;
    private Button mEntrarBtn, mIniciaConFacebookBtn;
    private TextView mOlvidoClaveTxv, mCrearCuentaTxv, mPoliticaDePrivacidadTxv;

    private static final String TAG = LoginActivity.class.getSimpleName();
    private int mRequestCodeCrearCuenta = 1;

    @Inject
    ViewModelFactory viewModelFactory;

    LoginViewModel loginViewModel;

    private void dataInjection() {
        AndroidInjection.inject(this);
        loginViewModel = ViewModelProviders.of(this, viewModelFactory).get(LoginViewModel.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        dataInjection();
        setViews();

        mEntrarBtn.setOnClickListener(v -> {
            //se envia el 'view' para despues poder colocar el mensajes de error
            RequestEntity requestEntity = crearRequestEntity();

            if (!validarFormularioView()) {

                loginViewModel.loginVendedor(requestEntity);
                loginViewModel.obtenerVendedor().observe(this, resource -> {
                    if (resource.isLoading()) {
                        // Show a progress spinner, and kick off a background task to
                        // perform the user register attempt.
                        showProgress(true);
                    } else if (resource.data != null) {
                        System.out.println("resource.data es diferente de nulo");
                        if (resource.data.getEstado() == PETICION_CORRECTA)
                            finish();

                        FancyToast.makeText(this, resource.data.getVendedor().getNombre(),
                                FancyToast.LENGTH_LONG, FancyToast.SUCCESS, true).show();

                    }
                    showProgress(false);
                });
            }
        });

        mOlvidoClaveTxv.setOnClickListener(v -> {
            launch_OlvidoClaveActivity();
        });

        mCrearCuentaTxv.setOnClickListener(v -> {
            launch_CrearCuentaActivity();
        });

    }

    private void setViews() {
        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
        mCorreoActxv = findViewById(R.id.atxv_correo);
        mPasswordPedtx = findViewById(R.id.pedtx_password);
        mEntrarBtn = findViewById(R.id.btn_entrar);
        mOlvidoClaveTxv = findViewById(R.id.txv_olvido_clave);
        mCrearCuentaTxv = findViewById(R.id.txv_crear_una_cuenta);
        mPoliticaDePrivacidadTxv = findViewById(R.id.txv_politica_de_privacidad);
        mIniciaConFacebookBtn = findViewById(R.id.btn_facebook_sign_in);
        mIniciaConFacebookBtn.setVisibility(View.GONE);

        ImageView imageViewLogo = findViewById(R.id.img_logo);
        imageViewLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCorreoActxv.setText("coba");
                mPasswordPedtx.setText("cobasoft");
            }
        });
    }

    private RequestEntity crearRequestEntity() {

        RequestEntity requestEntity = new RequestEntity();
        Vendedor vendedor = new Vendedor();
        // Store values at the time of the login attempt.
        String correo = mCorreoActxv.getText().toString();
        String password = mPasswordPedtx.getText().toString();

        vendedor.setCorreo(correo);
        vendedor.setPassword(password);

        requestEntity.setVendedor(vendedor);

        return requestEntity;
    }

    private void launch_OlvidoClaveActivity() {
        Intent intent = new Intent(this, OlvidoPasswordActivity.class);
        startActivityForResult(intent, mRequestCodeCrearCuenta);
    }

    private void launch_CrearCuentaActivity() {
        Intent intent = new Intent(this, CrearActualizarVendedorActivity.class);
        startActivityForResult(intent, mRequestCodeCrearCuenta);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == mRequestCodeCrearCuenta) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("result");
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    private boolean validarFormularioView() {//View rootView) {
        // Reset errors.
        mCorreoActxv.setError(null);
        mPasswordPedtx.setError(null);

        // Store values at the time of the login attempt.
        String correo = mCorreoActxv.getText().toString();
        String password = mPasswordPedtx.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordPedtx.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordPedtx;
            cancel = true;
        }

        // Check for a valid email address.
        if (TextUtils.isEmpty(correo)) {
            mCorreoActxv.setError(getString(R.string.error_field_required));
            focusView = mCorreoActxv;
            cancel = true;
        } else if (!isEmailValid(correo)) {
            mCorreoActxv.setError(getString(R.string.error_invalid_email));
            focusView = mCorreoActxv;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
        }
        return cancel;
    }

    private boolean isEmailValid(String email) {
        //TODO: Replace this with your own logic
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    private void showProgress(final boolean show) {
        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
    }

/*        @Override
        protected void onPostExecute(final LoginApiResponse loginApiResponse) {
            mAuthTask = null;

            try {
                if (loginApiResponse.getEstado() == estado_login_exitoso) {
                    showProgress(true);
                    //                   recuperaDelServidorYalmacenaLocalmente();
                    //                   Global.nombre_de_usuario = loginApiResponse.getMensaje();

                                       launch_MainActivity();

                    Toast.makeText(getApplicationContext(), loginApiResponse.getMensaje(), Toast.LENGTH_LONG).show();
                    finish();

                } else if (loginApiResponse.getEstado() == estado_login_fracaso) {
                    showProgress(false);
                    Snackbar.make(rootView, "Error de conexión ", Snackbar.LENGTH_INDEFINITE)
                            .setAction("VER ERROR ", view -> {
                                Toast.makeText(getApplicationContext(), loginApiResponse.getMensaje(), Toast.LENGTH_LONG).show();
                            }).show();
                }
            } catch (Exception e) {
                showProgress(false);
                Snackbar.make(rootView, "Error de conexión ", Snackbar.LENGTH_INDEFINITE)
                        .setAction("VER ERROR ", view -> {
                            Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
                        }).show();
            }
        }
*/

    private void launch_MainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);

    }

}
