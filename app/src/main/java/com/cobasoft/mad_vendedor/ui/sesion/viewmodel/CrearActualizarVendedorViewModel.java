package com.cobasoft.mad_vendedor.ui.sesion.viewmodel;

import androidx.lifecycle.MutableLiveData;

import com.cobasoft.mad_vendedor.data.local.dao.SesionDao;
import com.cobasoft.mad_vendedor.data.local.entity.RequestEntity;
import com.cobasoft.mad_vendedor.data.remote.api.ApiService;
import com.cobasoft.mad_vendedor.data.repository.SesionRepository;
import com.cobasoft.mad_vendedor.data.resource.Resource;
import com.cobasoft.mad_vendedor.ui.base.BaseViewModel;

import javax.inject.Inject;


public class CrearActualizarVendedorViewModel extends BaseViewModel {

    private SesionRepository sesionRepository;
    private MutableLiveData<Resource<RequestEntity>> vendedorLiveData = new MutableLiveData<>();

    @Inject
    public CrearActualizarVendedorViewModel(SesionDao sesionDao, ApiService apiService) {
        sesionRepository = new SesionRepository(sesionDao, apiService);
    }

    public MutableLiveData<Resource<RequestEntity>> obtenerVendedor() {
        return vendedorLiveData;
    }

    public void crearVendedor(RequestEntity requestEntity) {
        sesionRepository.crearActualizarVendedor(requestEntity)
                .doOnSubscribe(disposable -> addToDisposable(disposable))
                .subscribe(resource -> obtenerVendedor().postValue(resource));
    }

}
