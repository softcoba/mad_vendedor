package com.cobasoft.mad_vendedor;

public interface AppConstants {

    //cobasoft
    String ROOT_API_URL = "http://10.0.2.2:8080/mercado_a_domicilio/core/serviceVendedor/";

    String TMDB_API_KEY = "5e74ee79280d770dc8ed5a2fbdda955a";// esta es la clave del OAuth, que se reemplazaría, cuando ya se tenga el oauth

    String ROOM_DATABASE = "mad_comprador.db";

    Integer PETICION_CORRECTA = 200;
    Integer REDIRECCION = 300;
    Integer ERROR_DEL_CLIENTE = 400;
    Integer ERROR_DEL_SERVIDOR = 500;
}
