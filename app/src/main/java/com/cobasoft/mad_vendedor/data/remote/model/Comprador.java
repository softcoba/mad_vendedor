/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cobasoft.mad_vendedor.data.remote.model;


import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.TypeConverters;

import com.cobasoft.mad_vendedor.data.local.converter.DateTypeConverter;
import com.google.gson.annotations.Expose;

import java.util.Date;


public class Comprador implements Parcelable {

    @ColumnInfo(name = "ID_COMPRADOR")
    @Expose
    private Integer idComprador;
    @Expose
    private String nombre;
    @Expose
    private String apellido;
    @Expose
    private String celular;
    @Expose
    private String convencional;
    @Expose
    private String correo;
    @Expose
    private String domicilio;
    @ColumnInfo(name = "NUM_INTENTOS")
    private Integer numIntentos;
    @Expose
    private String password;
    @ColumnInfo(name = "FECHA_REGISTRO")
    @Expose
    @TypeConverters(DateTypeConverter.class)
    private Date fechaRegistro;
    @Expose
    private String estado;

    public Integer getIdComprador() {
        return idComprador;
    }

    public void setIdComprador(Integer idComprador) {
        this.idComprador = idComprador;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getConvencional() {
        return convencional;
    }

    public void setConvencional(String convencional) {
        this.convencional = convencional;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public Integer getNumIntentos() {
        return numIntentos;
    }

    public void setNumIntentos(Integer numIntentos) {
        this.numIntentos = numIntentos;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.idComprador);
        dest.writeString(this.nombre);
        dest.writeString(this.apellido);
        dest.writeString(this.celular);
        dest.writeString(this.convencional);
        dest.writeString(this.correo);
        dest.writeString(this.domicilio);
        dest.writeValue(this.numIntentos);
        dest.writeString(this.password);
        dest.writeLong(this.fechaRegistro != null ? this.fechaRegistro.getTime() : -1);
        dest.writeString(this.estado);
    }

    public Comprador() {
    }

    protected Comprador(Parcel in) {
        this.idComprador = (Integer) in.readValue(Integer.class.getClassLoader());
        this.nombre = in.readString();
        this.apellido = in.readString();
        this.celular = in.readString();
        this.convencional = in.readString();
        this.correo = in.readString();
        this.domicilio = in.readString();
        this.numIntentos = (Integer) in.readValue(Integer.class.getClassLoader());
        this.password = in.readString();
        long tmpFechaRegistro = in.readLong();
        this.fechaRegistro = tmpFechaRegistro == -1 ? null : new Date(tmpFechaRegistro);
        this.estado = in.readString();
    }

    public static final Creator<Comprador> CREATOR = new Creator<Comprador>() {
        @Override
        public Comprador createFromParcel(Parcel source) {
            return new Comprador(source);
        }

        @Override
        public Comprador[] newArray(int size) {
            return new Comprador[size];
        }
    };
}
