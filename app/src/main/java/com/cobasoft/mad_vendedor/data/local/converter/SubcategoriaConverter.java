package com.cobasoft.mad_vendedor.data.local.converter;

import androidx.room.TypeConverter;

import com.cobasoft.mad_vendedor.data.remote.model.Categoria;
import com.cobasoft.mad_vendedor.data.remote.model.Subcategoria;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class SubcategoriaConverter {

    @TypeConverter
    public Subcategoria fromString(String value) {
        Type type= new TypeToken<Categoria>() {
        }.getType();
        Subcategoria subcategoria = new Gson().fromJson(value, type);
        return subcategoria;
    }

    @TypeConverter
    public String fromList(Subcategoria subcategoria) {
        return new Gson().toJson(subcategoria);
    }
}
