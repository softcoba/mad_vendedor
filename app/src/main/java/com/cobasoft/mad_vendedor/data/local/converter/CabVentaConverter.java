package com.cobasoft.mad_vendedor.data.local.converter;

import androidx.room.TypeConverter;

import com.cobasoft.mad_vendedor.data.remote.model.CabVenta;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class CabVentaConverter {

    @TypeConverter
    public CabVenta fromString(String value) {
        Type type= new TypeToken<CabVenta>() {
        }.getType();
        CabVenta cabVenta = new Gson().fromJson(value, type);
        return cabVenta;
    }

    @TypeConverter
    public String fromList(CabVenta cabVenta) {
        return new Gson().toJson(cabVenta);
    }
}
