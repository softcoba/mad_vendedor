package com.cobasoft.mad_vendedor.data.local.converter;

import androidx.room.TypeConverter;

import com.cobasoft.mad_vendedor.data.remote.model.Categoria;
import com.cobasoft.mad_vendedor.data.remote.model.Motorizado;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class MotorizadoConverter {

    @TypeConverter
    public Motorizado fromString(String value) {
        Type type= new TypeToken<Categoria>() {
        }.getType();
        Motorizado motorizado = new Gson().fromJson(value, type);
        return motorizado;
    }

    @TypeConverter
    public String fromList(Motorizado motorizado) {
        return new Gson().toJson(motorizado);
    }
}
