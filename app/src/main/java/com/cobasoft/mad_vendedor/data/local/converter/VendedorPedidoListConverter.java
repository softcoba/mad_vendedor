package com.cobasoft.mad_vendedor.data.local.converter;

import androidx.room.TypeConverter;

import com.cobasoft.mad_vendedor.data.remote.model.VendedorPedido;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class VendedorPedidoListConverter {

    @TypeConverter
    public List<VendedorPedido> fromString(String value) {
        Type listType = new TypeToken<List<VendedorPedido>>() {}.getType();
        List<VendedorPedido> casts = new Gson().fromJson(value, listType);
        return casts;
    }

    @TypeConverter
    public String fromList(List<VendedorPedido> casts) {
        return new Gson().toJson(casts);
    }
}
