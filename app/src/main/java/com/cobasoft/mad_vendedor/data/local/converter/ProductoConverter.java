package com.cobasoft.mad_vendedor.data.local.converter;

import androidx.room.TypeConverter;

import com.cobasoft.mad_vendedor.data.remote.model.Categoria;
import com.cobasoft.mad_vendedor.data.remote.model.Producto;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class ProductoConverter {

    @TypeConverter
    public Producto fromString(String value) {
        Type type= new TypeToken<Categoria>() {
        }.getType();
        Producto producto = new Gson().fromJson(value, type);
        return producto;
    }

    @TypeConverter
    public String fromList(Producto producto) {
        return new Gson().toJson(producto);
    }
}
