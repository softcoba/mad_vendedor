/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cobasoft.mad_vendedor.data.remote.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.TypeConverters;

import com.cobasoft.mad_vendedor.data.local.converter.DateTypeConverter;
import com.google.gson.annotations.Expose;

import java.util.Date;


public class VendedorPedido implements Parcelable {

    @ColumnInfo(name = "ID_VENDEDOR_PEDIDO")
    @Expose
    private Integer idVendedorPedido;
    @ColumnInfo(name = "FECHA_PEDIDO")
    @Expose
    @TypeConverters(DateTypeConverter.class)
    private Date fechaPedido;
    @ColumnInfo(name = "FECHA_ENTREGA")
    @Expose
    @TypeConverters(DateTypeConverter.class)
    private Date fechaEntrega;
    @ColumnInfo(name = "STATUS_PEDIDO")
    @Expose
    private String statusPedido;
    @Expose
    private String observacion;
    @ColumnInfo(name = "ID_CAB_VENTA")
    @Expose
    private Integer idCabVenta;
    @ColumnInfo(name = "ID_FORMA_DE_PAGO")
    @Expose
    private Integer idFormaDePago;
    @ColumnInfo(name = "ID_VENDEDOR")
    @Expose
    private Integer idVendedor;

    public Integer getIdVendedorPedido() {
        return idVendedorPedido;
    }

    public void setIdVendedorPedido(Integer idVendedorPedido) {
        this.idVendedorPedido = idVendedorPedido;
    }

    public Date getFechaPedido() {
        return fechaPedido;
    }

    public void setFechaPedido(Date fechaPedido) {
        this.fechaPedido = fechaPedido;
    }

    public Date getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(Date fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    public String getStatusPedido() {
        return statusPedido;
    }

    public void setStatusPedido(String statusPedido) {
        this.statusPedido = statusPedido;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Integer getIdCabVenta() {
        return idCabVenta;
    }

    public void setIdCabVenta(Integer idCabVenta) {
        this.idCabVenta = idCabVenta;
    }

    public Integer getIdFormaDePago() {
        return idFormaDePago;
    }

    public void setIdFormaDePago(Integer idFormaDePago) {
        this.idFormaDePago = idFormaDePago;
    }

    public Integer getIdVendedor() {
        return idVendedor;
    }

    public void setIdVendedor(Integer idVendedor) {
        this.idVendedor = idVendedor;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.idVendedorPedido);
        dest.writeLong(this.fechaPedido != null ? this.fechaPedido.getTime() : -1);
        dest.writeLong(this.fechaEntrega != null ? this.fechaEntrega.getTime() : -1);
        dest.writeString(this.statusPedido);
        dest.writeString(this.observacion);
        dest.writeValue(this.idCabVenta);
        dest.writeValue(this.idFormaDePago);
        dest.writeValue(this.idVendedor);
    }

    public VendedorPedido() {
    }

    protected VendedorPedido(Parcel in) {
        this.idVendedorPedido = (Integer) in.readValue(Integer.class.getClassLoader());
        long tmpFechaPedido = in.readLong();
        this.fechaPedido = tmpFechaPedido == -1 ? null : new Date(tmpFechaPedido);
        long tmpFechaEntrega = in.readLong();
        this.fechaEntrega = tmpFechaEntrega == -1 ? null : new Date(tmpFechaEntrega);
        this.statusPedido = in.readString();
        this.observacion = in.readString();
        this.idCabVenta = (Integer) in.readValue(Integer.class.getClassLoader());
        this.idFormaDePago = (Integer) in.readValue(Integer.class.getClassLoader());
        this.idVendedor = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Creator<VendedorPedido> CREATOR = new Creator<VendedorPedido>() {
        @Override
        public VendedorPedido createFromParcel(Parcel source) {
            return new VendedorPedido(source);
        }

        @Override
        public VendedorPedido[] newArray(int size) {
            return new VendedorPedido[size];
        }
    };
}
