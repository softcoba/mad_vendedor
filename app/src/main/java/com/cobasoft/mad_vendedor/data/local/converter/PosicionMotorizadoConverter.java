package com.cobasoft.mad_vendedor.data.local.converter;

import androidx.room.TypeConverter;

import com.cobasoft.mad_vendedor.data.remote.model.Categoria;
import com.cobasoft.mad_vendedor.data.remote.model.PosicionMotorizado;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class PosicionMotorizadoConverter {

    @TypeConverter
    public PosicionMotorizado fromString(String value) {
        Type type= new TypeToken<Categoria>() {
        }.getType();
        PosicionMotorizado posicionMotorizado = new Gson().fromJson(value, type);
        return posicionMotorizado;
    }

    @TypeConverter
    public String fromList(PosicionMotorizado posicionMotorizado) {
        return new Gson().toJson(posicionMotorizado);
    }
}
