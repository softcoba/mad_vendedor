/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cobasoft.mad_vendedor.data.remote.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.Entity;

import com.google.gson.annotations.Expose;

@Entity(tableName = "mensaje_correo")
public class MensajeCorreo implements Parcelable {

    @ColumnInfo(name = "ID_MENSAJE_CORREO")
    @Expose
    private Integer idMensajeCorreo;
    @Expose
    private String nombre;
    @Expose
    private String titulo;
    @Expose
    private String contenido;
    @Expose
    private String configuracion;

    public Integer getIdMensajeCorreo() {
        return idMensajeCorreo;
    }

    public void setIdMensajeCorreo(Integer idMensajeCorreo) {
        this.idMensajeCorreo = idMensajeCorreo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getContenido() {
        return contenido;
    }

    public void setContenido(String contenido) {
        this.contenido = contenido;
    }

    public String getConfiguracion() {
        return configuracion;
    }

    public void setConfiguracion(String configuracion) {
        this.configuracion = configuracion;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.idMensajeCorreo);
        dest.writeString(this.nombre);
        dest.writeString(this.titulo);
        dest.writeString(this.contenido);
        dest.writeString(this.configuracion);
    }

    public MensajeCorreo() {
    }

    protected MensajeCorreo(Parcel in) {
        this.idMensajeCorreo = (Integer) in.readValue(Integer.class.getClassLoader());
        this.nombre = in.readString();
        this.titulo = in.readString();
        this.contenido = in.readString();
        this.configuracion = in.readString();
    }

    public static final Creator<MensajeCorreo> CREATOR = new Creator<MensajeCorreo>() {
        @Override
        public MensajeCorreo createFromParcel(Parcel source) {
            return new MensajeCorreo(source);
        }

        @Override
        public MensajeCorreo[] newArray(int size) {
            return new MensajeCorreo[size];
        }
    };
}
