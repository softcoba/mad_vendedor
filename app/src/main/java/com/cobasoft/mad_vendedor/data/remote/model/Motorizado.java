/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cobasoft.mad_vendedor.data.remote.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.TypeConverters;

import com.cobasoft.mad_vendedor.data.local.converter.DateTypeConverter;
import com.google.gson.annotations.Expose;

import java.util.Date;


public class Motorizado implements Parcelable {

    @ColumnInfo(name = "ID_MOTORIZADO")
    @Expose
    private Integer idMotorizado;
    @Expose
    private String nombre;
    @Expose
    private String apellido;
    @Expose
    private String cedula;
    @Expose
    private String celular;
    @Expose
    private String placa;
    @Expose
    private String password;
    @ColumnInfo(name = "FECHA_REGISTRO")
    @Expose
    @TypeConverters(DateTypeConverter.class)
    private Date fechaRegistro;

    public Integer getIdMotorizado() {
        return idMotorizado;
    }

    public void setIdMotorizado(Integer idMotorizado) {
        this.idMotorizado = idMotorizado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.idMotorizado);
        dest.writeString(this.nombre);
        dest.writeString(this.apellido);
        dest.writeString(this.cedula);
        dest.writeString(this.celular);
        dest.writeString(this.placa);
        dest.writeString(this.password);
        dest.writeLong(this.fechaRegistro != null ? this.fechaRegistro.getTime() : -1);
    }

    public Motorizado() {
    }

    protected Motorizado(Parcel in) {
        this.idMotorizado = (Integer) in.readValue(Integer.class.getClassLoader());
        this.nombre = in.readString();
        this.apellido = in.readString();
        this.cedula = in.readString();
        this.celular = in.readString();
        this.placa = in.readString();
        this.password = in.readString();
        long tmpFechaRegistro = in.readLong();
        this.fechaRegistro = tmpFechaRegistro == -1 ? null : new Date(tmpFechaRegistro);
    }

    public static final Creator<Motorizado> CREATOR = new Creator<Motorizado>() {
        @Override
        public Motorizado createFromParcel(Parcel source) {
            return new Motorizado(source);
        }

        @Override
        public Motorizado[] newArray(int size) {
            return new Motorizado[size];
        }
    };
}
