package com.cobasoft.mad_vendedor.data.local.converter;

import androidx.room.TypeConverter;

import com.cobasoft.mad_vendedor.data.remote.model.CalificaProducto;
import com.cobasoft.mad_vendedor.data.remote.model.Categoria;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class CalificaProductoConverter {

    @TypeConverter
    public CalificaProducto fromString(String value) {
        Type type= new TypeToken<Categoria>() {
        }.getType();
        CalificaProducto calificaProducto = new Gson().fromJson(value, type);
        return calificaProducto;
    }

    @TypeConverter
    public String fromList(CalificaProducto calificaProducto) {
        return new Gson().toJson(calificaProducto);
    }
}
