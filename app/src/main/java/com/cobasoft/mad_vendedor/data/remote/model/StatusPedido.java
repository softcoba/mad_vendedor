/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cobasoft.mad_vendedor.data.remote.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;

import com.google.gson.annotations.Expose;


public class StatusPedido implements Parcelable {

    @Expose
    @ColumnInfo(name = "ID_TIPO_STATUS")
    private Integer idTipoStatus;
    @Expose
    private String nombre;

    public Integer getIdTipoStatus() {
        return idTipoStatus;
    }

    public void setIdTipoStatus(Integer idTipoStatus) {
        this.idTipoStatus = idTipoStatus;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.idTipoStatus);
        dest.writeString(this.nombre);
    }

    public StatusPedido() {
    }

    protected StatusPedido(Parcel in) {
        this.idTipoStatus = (Integer) in.readValue(Integer.class.getClassLoader());
        this.nombre = in.readString();
    }

    public static final Parcelable.Creator<StatusPedido> CREATOR = new Parcelable.Creator<StatusPedido>() {
        @Override
        public StatusPedido createFromParcel(Parcel source) {
            return new StatusPedido(source);
        }

        @Override
        public StatusPedido[] newArray(int size) {
            return new StatusPedido[size];
        }
    };
}
