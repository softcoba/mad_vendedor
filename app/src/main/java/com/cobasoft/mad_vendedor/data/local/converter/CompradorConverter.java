package com.cobasoft.mad_vendedor.data.local.converter;

import androidx.room.TypeConverter;

import com.cobasoft.mad_vendedor.data.remote.model.Categoria;
import com.cobasoft.mad_vendedor.data.remote.model.Comprador;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class CompradorConverter {

    @TypeConverter
    public Comprador fromString(String value) {
        Type type= new TypeToken<Categoria>() {
        }.getType();
        Comprador comprador = new Gson().fromJson(value, type);
        return comprador;
    }

    @TypeConverter
    public String fromList(Comprador comprador) {
        return new Gson().toJson(comprador);
    }
}
