package com.cobasoft.mad_vendedor.data.local.converter;

import androidx.room.TypeConverter;

import com.cobasoft.mad_vendedor.data.remote.model.Categoria;
import com.cobasoft.mad_vendedor.data.remote.model.Impuesto;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class ImpuestoConverter {

    @TypeConverter
    public Impuesto fromString(String value) {
        Type type= new TypeToken<Categoria>() {
        }.getType();
        Impuesto impuesto = new Gson().fromJson(value, type);
        return impuesto;
    }

    @TypeConverter
    public String fromList(Impuesto impuesto) {
        return new Gson().toJson(impuesto);
    }
}
