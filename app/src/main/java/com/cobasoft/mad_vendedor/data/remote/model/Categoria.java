/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cobasoft.mad_vendedor.data.remote.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;

import com.google.gson.annotations.Expose;

import java.util.Date;


public class Categoria implements Parcelable {

    @ColumnInfo(name = "ID_CATEGORIA")
    @Expose
    private Integer idCategoria;
    @Expose
    private String nombre;
    @Expose
    @ColumnInfo(name = "UBICACION_FOTO")
    private String ubicacionFoto;
    @ColumnInfo(name = "FECHA_REGISTRO")
    @Expose
    private Date fechaRegistro;

    public Integer getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Integer idCategoria) {
        this.idCategoria = idCategoria;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUbicacionFoto() {
        return ubicacionFoto;
    }

    public void setUbicacionFoto(String ubicacionFoto) {
        this.ubicacionFoto = ubicacionFoto;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.idCategoria);
        dest.writeString(this.nombre);
        dest.writeString(this.ubicacionFoto);
        dest.writeLong(this.fechaRegistro != null ? this.fechaRegistro.getTime() : -1);
    }

    public Categoria() {
    }

    protected Categoria(Parcel in) {
        this.idCategoria = (Integer) in.readValue(Integer.class.getClassLoader());
        this.nombre = in.readString();
        this.ubicacionFoto = in.readString();
        long tmpFechaRegistro = in.readLong();
        this.fechaRegistro = tmpFechaRegistro == -1 ? null : new Date(tmpFechaRegistro);
    }

    public static final Parcelable.Creator<Categoria> CREATOR = new Parcelable.Creator<Categoria>() {
        @Override
        public Categoria createFromParcel(Parcel source) {
            return new Categoria(source);
        }

        @Override
        public Categoria[] newArray(int size) {
            return new Categoria[size];
        }
    };
}
