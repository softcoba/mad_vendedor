/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cobasoft.mad_vendedor.data.remote.model;


import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;

import com.google.gson.annotations.Expose;


public class Pregunta implements Parcelable {

    @ColumnInfo(name = "ID_PREGUNTA")
    @Expose
    private Integer idPregunta;
    @Expose
    private String pregunta;
    @ColumnInfo(name = "ID_COMPRADOR")
    @Expose
    private Integer idComprador;

    public Integer getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(Integer idPregunta) {
        this.idPregunta = idPregunta;
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public Integer getIdComprador() {
        return idComprador;
    }

    public void setIdComprador(Integer idComprador) {
        this.idComprador = idComprador;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.idPregunta);
        dest.writeString(this.pregunta);
        dest.writeValue(this.idComprador);
    }

    public Pregunta() {
    }

    protected Pregunta(Parcel in) {
        this.idPregunta = (Integer) in.readValue(Integer.class.getClassLoader());
        this.pregunta = in.readString();
        this.idComprador = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Parcelable.Creator<Pregunta> CREATOR = new Parcelable.Creator<Pregunta>() {
        @Override
        public Pregunta createFromParcel(Parcel source) {
            return new Pregunta(source);
        }

        @Override
        public Pregunta[] newArray(int size) {
            return new Pregunta[size];
        }
    };
}
