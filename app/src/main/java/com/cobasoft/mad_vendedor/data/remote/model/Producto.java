/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cobasoft.mad_vendedor.data.remote.model;


import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.TypeConverters;

import com.cobasoft.mad_vendedor.data.local.converter.DateTypeConverter;
import com.google.gson.annotations.Expose;

import java.util.Date;


public class Producto implements Parcelable {

    @ColumnInfo(name = "ID_PRODUCTO")
    @Expose
    private Integer idProducto;
    @Expose
    private String nombre;
    @Expose
    private String descripcion;
    @ColumnInfo(name = "PRECIO_UNITARIO")
    @Expose
    private Double precioUnitario;
    @ColumnInfo(name = "STOCK_MINIMO")
    @Expose
    private Integer stockMinimo;
    @Expose
    private Integer stock;
    @ColumnInfo(name = "FECHA_REGISTRO")
    @Expose
    @TypeConverters(DateTypeConverter.class)
    private Date fechaRegistro;
    @ColumnInfo(name = "ID_CATEGORIA")
    @Expose
    private Integer idCategoria;
    @ColumnInfo(name = "ID_IMPUESTO")
    @Expose
    private Integer idImpuesto;
    @ColumnInfo(name = "ID_FOTO_PRODUCTO")
    @Expose
    private Integer idFotoProducto;
    @ColumnInfo(name = "ID_VENDEDOR")
    @Expose
    private Integer idVendedor;

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Double getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(Double precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public Integer getStockMinimo() {
        return stockMinimo;
    }

    public void setStockMinimo(Integer stockMinimo) {
        this.stockMinimo = stockMinimo;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Integer getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Integer idCategoria) {
        this.idCategoria = idCategoria;
    }

    public Integer getIdImpuesto() {
        return idImpuesto;
    }

    public void setIdImpuesto(Integer idImpuesto) {
        this.idImpuesto = idImpuesto;
    }

    public Integer getIdFotoProducto() {
        return idFotoProducto;
    }

    public void setIdFotoProducto(Integer idFotoProducto) {
        this.idFotoProducto = idFotoProducto;
    }

    public Integer getIdVendedor() {
        return idVendedor;
    }

    public void setIdVendedor(Integer idVendedor) {
        this.idVendedor = idVendedor;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.idProducto);
        dest.writeString(this.nombre);
        dest.writeString(this.descripcion);
        dest.writeValue(this.precioUnitario);
        dest.writeValue(this.stockMinimo);
        dest.writeValue(this.stock);
        dest.writeLong(this.fechaRegistro != null ? this.fechaRegistro.getTime() : -1);
        dest.writeValue(this.idCategoria);
        dest.writeValue(this.idImpuesto);
        dest.writeValue(this.idFotoProducto);
        dest.writeValue(this.idVendedor);
    }

    public Producto() {
    }

    protected Producto(Parcel in) {
        this.idProducto = (Integer) in.readValue(Integer.class.getClassLoader());
        this.nombre = in.readString();
        this.descripcion = in.readString();
        this.precioUnitario = (Double) in.readValue(Double.class.getClassLoader());
        this.stockMinimo = (Integer) in.readValue(Integer.class.getClassLoader());
        this.stock = (Integer) in.readValue(Integer.class.getClassLoader());
        long tmpFechaRegistro = in.readLong();
        this.fechaRegistro = tmpFechaRegistro == -1 ? null : new Date(tmpFechaRegistro);
        this.idCategoria = (Integer) in.readValue(Integer.class.getClassLoader());
        this.idImpuesto = (Integer) in.readValue(Integer.class.getClassLoader());
        this.idFotoProducto = (Integer) in.readValue(Integer.class.getClassLoader());
        this.idVendedor = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Creator<Producto> CREATOR = new Creator<Producto>() {
        @Override
        public Producto createFromParcel(Parcel source) {
            return new Producto(source);
        }

        @Override
        public Producto[] newArray(int size) {
            return new Producto[size];
        }
    };
}

