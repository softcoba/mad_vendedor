/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cobasoft.mad_vendedor.data.remote.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.TypeConverters;

import com.cobasoft.mad_vendedor.data.local.converter.DateTypeConverter;
import com.google.gson.annotations.Expose;

import java.util.Date;


public class PosicionMotorizado implements Parcelable {

    @ColumnInfo(name = "ID_POSICION_MOTORIZADO")
    @Expose
    private Integer idPosicionMotorizado;
    @Expose
    private Double latitud;
    @Expose
    private Double longitud;
    @ColumnInfo(name = "FECHA_REGISTRO")
    @Expose
    @TypeConverters(DateTypeConverter.class)
    private Date fechaRegistro;
    @ColumnInfo(name = "ID_MOTORIZADO_PEDIDO")
    @Expose
    private Integer idMotorizadoPedido;

    public Integer getIdPosicionMotorizado() {
        return idPosicionMotorizado;
    }

    public void setIdPosicionMotorizado(Integer idPosicionMotorizado) {
        this.idPosicionMotorizado = idPosicionMotorizado;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Integer getIdMotorizadoPedido() {
        return idMotorizadoPedido;
    }

    public void setIdMotorizadoPedido(Integer idMotorizadoPedido) {
        this.idMotorizadoPedido = idMotorizadoPedido;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.idPosicionMotorizado);
        dest.writeValue(this.latitud);
        dest.writeValue(this.longitud);
        dest.writeLong(this.fechaRegistro != null ? this.fechaRegistro.getTime() : -1);
        dest.writeValue(this.idMotorizadoPedido);
    }

    public PosicionMotorizado() {
    }

    protected PosicionMotorizado(Parcel in) {
        this.idPosicionMotorizado = (Integer) in.readValue(Integer.class.getClassLoader());
        this.latitud = (Double) in.readValue(Double.class.getClassLoader());
        this.longitud = (Double) in.readValue(Double.class.getClassLoader());
        long tmpFechaRegistro = in.readLong();
        this.fechaRegistro = tmpFechaRegistro == -1 ? null : new Date(tmpFechaRegistro);
        this.idMotorizadoPedido = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Creator<PosicionMotorizado> CREATOR = new Creator<PosicionMotorizado>() {
        @Override
        public PosicionMotorizado createFromParcel(Parcel source) {
            return new PosicionMotorizado(source);
        }

        @Override
        public PosicionMotorizado[] newArray(int size) {
            return new PosicionMotorizado[size];
        }
    };
}
