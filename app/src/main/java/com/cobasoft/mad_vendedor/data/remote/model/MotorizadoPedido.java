/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cobasoft.mad_vendedor.data.remote.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;

import com.google.gson.annotations.Expose;


public class MotorizadoPedido implements Parcelable {

    @ColumnInfo(name = "ID_MOTORIZADO_PEDIDO")
    @Expose
    private Integer idMotorizadoPedido;
    @ColumnInfo(name = "STATUS_PEDIDO")
    @Expose
    private String statusPedido;
    @Expose
    private String observacion;
    @ColumnInfo(name = "ID_MOTORIZADO")
    @Expose
    private Integer idMotorizado;

    public Integer getIdMotorizadoPedido() {
        return idMotorizadoPedido;
    }

    public void setIdMotorizadoPedido(Integer idMotorizadoPedido) {
        this.idMotorizadoPedido = idMotorizadoPedido;
    }

    public String getStatusPedido() {
        return statusPedido;
    }

    public void setStatusPedido(String statusPedido) {
        this.statusPedido = statusPedido;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Integer getIdMotorizado() {
        return idMotorizado;
    }

    public void setIdMotorizado(Integer idMotorizado) {
        this.idMotorizado = idMotorizado;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.idMotorizadoPedido);
        dest.writeString(this.statusPedido);
        dest.writeString(this.observacion);
        dest.writeValue(this.idMotorizado);
    }

    public MotorizadoPedido() {
    }

    protected MotorizadoPedido(Parcel in) {
        this.idMotorizadoPedido = (Integer) in.readValue(Integer.class.getClassLoader());
        this.statusPedido = in.readString();
        this.observacion = in.readString();
        this.idMotorizado = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Creator<MotorizadoPedido> CREATOR = new Creator<MotorizadoPedido>() {
        @Override
        public MotorizadoPedido createFromParcel(Parcel source) {
            return new MotorizadoPedido(source);
        }

        @Override
        public MotorizadoPedido[] newArray(int size) {
            return new MotorizadoPedido[size];
        }
    };
}
