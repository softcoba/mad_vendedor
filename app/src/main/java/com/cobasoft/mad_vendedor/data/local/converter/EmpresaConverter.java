package com.cobasoft.mad_vendedor.data.local.converter;

import androidx.room.TypeConverter;

import com.cobasoft.mad_vendedor.data.remote.model.Categoria;
import com.cobasoft.mad_vendedor.data.remote.model.Empresa;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class EmpresaConverter {

    @TypeConverter
    public Empresa fromString(String value) {
        Type type= new TypeToken<Categoria>() {
        }.getType();
        Empresa empresa = new Gson().fromJson(value, type);
        return empresa;
    }

    @TypeConverter
    public String fromList(Empresa empresa) {
        return new Gson().toJson(empresa);
    }
}
