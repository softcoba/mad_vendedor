package com.cobasoft.mad_vendedor.data.local.converter;

import androidx.room.TypeConverter;

import com.cobasoft.mad_vendedor.data.remote.model.Categoria;
import com.cobasoft.mad_vendedor.data.remote.model.PregRespProducto;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class PregRespProductoConverter {

    @TypeConverter
    public PregRespProducto fromString(String value) {
        Type type= new TypeToken<Categoria>() {
        }.getType();
        PregRespProducto pregRespProducto = new Gson().fromJson(value, type);
        return pregRespProducto;
    }

    @TypeConverter
    public String fromList(PregRespProducto pregRespProducto) {
        return new Gson().toJson(pregRespProducto);
    }
}
