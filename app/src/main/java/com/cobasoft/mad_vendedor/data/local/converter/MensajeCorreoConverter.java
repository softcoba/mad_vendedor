package com.cobasoft.mad_vendedor.data.local.converter;

import androidx.room.TypeConverter;

import com.cobasoft.mad_vendedor.data.remote.model.Categoria;
import com.cobasoft.mad_vendedor.data.remote.model.MensajeCorreo;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class MensajeCorreoConverter {

    @TypeConverter
    public MensajeCorreo fromString(String value) {
        Type type= new TypeToken<Categoria>() {
        }.getType();
        MensajeCorreo mensajeCorreo = new Gson().fromJson(value, type);
        return mensajeCorreo;
    }

    @TypeConverter
    public String fromList(MensajeCorreo mensajeCorreo) {
        return new Gson().toJson(mensajeCorreo);
    }
}
