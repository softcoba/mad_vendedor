package com.cobasoft.mad_vendedor.data.local.converter;

import androidx.room.TypeConverter;

import com.cobasoft.mad_vendedor.data.remote.model.CalificaComprador;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class CalificaCompradorConverter {

    @TypeConverter
    public CalificaComprador fromString(String value) {
        Type type= new TypeToken<CalificaComprador>() {
        }.getType();
        CalificaComprador calificaComprador = new Gson().fromJson(value, type);
        return calificaComprador;
    }

    @TypeConverter
    public String fromList(CalificaComprador calificaComprador) {
        return new Gson().toJson(calificaComprador);
    }
}
