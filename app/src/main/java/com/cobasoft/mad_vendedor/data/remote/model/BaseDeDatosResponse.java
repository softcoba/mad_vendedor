package com.cobasoft.mad_vendedor.data.remote.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BaseDeDatosResponse {

	@SerializedName("id_log_vendedor")
	@Expose
	private Integer idLogVendedor;

	@Expose
	private String mensaje;

	@Expose
	private Integer estado;

	@Expose
	private CabVenta cabVenta;
	@Expose
	private CalificaComprador calificaComprador;
	@Expose
	private CalificaMotorizado calificaMotorizado;
	@Expose
	private CalificaProducto calificaProducto;
	@Expose
	private Categoria categoria;
	@Expose
	private Comprador comprador;
	@Expose
	private DetVenta detVenta;
	@Expose
	private Empresa empresa;
	@Expose
	private FormaDePago formaDePago;
	@Expose
	private FotoProducto fotoProducto;
	@Expose
	private Impuesto impuesto;
	@Expose
	private MensajeCorreo mensajeCorreo;
	@Expose
	private Motorizado motorizado;
	@Expose
	private MotorizadoPedido motorizadoPedido;
	@Expose
	private PedidoComprador pedidoComprador;
	@Expose
	private PosicionMotorizado posicionMotorizado;
	@Expose
	private PregRespProducto pregRespProducto;
	@Expose
	private Pregunta pregunta;
	@Expose
	private Producto producto;
	@Expose
	private Respuesta respuesta;
	@Expose
	private StatusPedido statusPedido;
	@Expose
	private Subcategoria subcategoria;
	@Expose
	private Vendedor vendedor;
	@Expose
	private VendedorPedido vendedorPedido;

	public Integer getIdLogVendedor() {	return idLogVendedor;}

	public void setIdLogVendedor(Integer idLogVendedor) {this.idLogVendedor = idLogVendedor;}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public CabVenta getCabVenta() {
		return cabVenta;
	}

	public void setCabVenta(CabVenta cabVenta) {
		this.cabVenta = cabVenta;
	}

	public CalificaComprador getCalificaComprador() {
		return calificaComprador;
	}

	public void setCalificaComprador(CalificaComprador calificaComprador) {
		this.calificaComprador = calificaComprador;
	}

	public CalificaMotorizado getCalificaMotorizado() {
		return calificaMotorizado;
	}

	public void setCalificaMotorizado(CalificaMotorizado calificaMotorizado) {
		this.calificaMotorizado = calificaMotorizado;
	}

	public CalificaProducto getCalificaProducto() {
		return calificaProducto;
	}

	public void setCalificaProducto(CalificaProducto calificaProducto) {
		this.calificaProducto = calificaProducto;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public Comprador getComprador() {
		return comprador;
	}

	public void setComprador(Comprador comprador) {
		this.comprador = comprador;
	}

	public DetVenta getDetVenta() {
		return detVenta;
	}

	public void setDetVenta(DetVenta detVenta) {
		this.detVenta = detVenta;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public FormaDePago getFormaDePago() {
		return formaDePago;
	}

	public void setFormaDePago(FormaDePago formaDePago) {
		this.formaDePago = formaDePago;
	}

	public FotoProducto getFotoProducto() {
		return fotoProducto;
	}

	public void setFotoProducto(FotoProducto fotoProducto) {
		this.fotoProducto = fotoProducto;
	}

	public Impuesto getImpuesto() {
		return impuesto;
	}

	public void setImpuesto(Impuesto impuesto) {
		this.impuesto = impuesto;
	}

	public MensajeCorreo getMensajeCorreo() {
		return mensajeCorreo;
	}

	public void setMensajeCorreo(MensajeCorreo mensajeCorreo) {
		this.mensajeCorreo = mensajeCorreo;
	}

	public Motorizado getMotorizado() {
		return motorizado;
	}

	public void setMotorizado(Motorizado motorizado) {
		this.motorizado = motorizado;
	}

	public MotorizadoPedido getMotorizadoPedido() {
		return motorizadoPedido;
	}

	public void setMotorizadoPedido(MotorizadoPedido motorizadoPedido) {
		this.motorizadoPedido = motorizadoPedido;
	}

	public PedidoComprador getPedidoComprador() {
		return pedidoComprador;
	}

	public void setPedidoComprador(PedidoComprador pedidoComprador) {
		this.pedidoComprador = pedidoComprador;
	}

	public PosicionMotorizado getPosicionMotorizado() {
		return posicionMotorizado;
	}

	public void setPosicionMotorizado(PosicionMotorizado posicionMotorizado) {
		this.posicionMotorizado = posicionMotorizado;
	}

	public PregRespProducto getPregRespProducto() {
		return pregRespProducto;
	}

	public void setPregRespProducto(PregRespProducto pregRespProducto) {
		this.pregRespProducto = pregRespProducto;
	}

	public Pregunta getPregunta() {
		return pregunta;
	}

	public void setPregunta(Pregunta pregunta) {
		this.pregunta = pregunta;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public Respuesta getRespuesta() {
		return respuesta;
	}

	public void setRespuesta(Respuesta respuesta) {
		this.respuesta = respuesta;
	}

	public StatusPedido getStatusPedido() {
		return statusPedido;
	}

	public void setStatusPedido(StatusPedido statusPedido) {
		this.statusPedido = statusPedido;
	}

	public Subcategoria getSubcategoria() {
		return subcategoria;
	}

	public void setSubcategoria(Subcategoria subcategoria) {
		this.subcategoria = subcategoria;
	}

	public Vendedor getVendedor() {
		return vendedor;
	}

	public void setVendedor(Vendedor vendedor) {
		this.vendedor = vendedor;
	}

	public VendedorPedido getVendedorPedido() {
		return vendedorPedido;
	}

	public void setVendedorPedido(VendedorPedido vendedorPedido) {
		this.vendedorPedido = vendedorPedido;
	}

}
