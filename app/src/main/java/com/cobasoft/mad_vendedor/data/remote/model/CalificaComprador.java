/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cobasoft.mad_vendedor.data.remote.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;

import com.google.gson.annotations.Expose;


public class CalificaComprador implements Parcelable {

    @ColumnInfo(name = "ID_CALIFICA_COMPRADOR")
    @Expose
    private Integer idCalificaComprador;
    @Expose
    private Integer valor;
    @Expose
    private String comentario;
    @ColumnInfo(name = "ID_MOTORIZADO_PEDIDO")
    @Expose
    private Integer idMotorizadoPedido;

    public Integer getIdCalificaComprador() {
        return idCalificaComprador;
    }

    public void setIdCalificaComprador(Integer idCalificaComprador) {
        this.idCalificaComprador = idCalificaComprador;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Integer getIdMotorizadoPedido() {
        return idMotorizadoPedido;
    }

    public void setIdMotorizadoPedido(Integer idMotorizadoPedido) {
        this.idMotorizadoPedido = idMotorizadoPedido;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.idCalificaComprador);
        dest.writeValue(this.valor);
        dest.writeString(this.comentario);
        dest.writeValue(this.idMotorizadoPedido);
    }

    public CalificaComprador() {
    }

    protected CalificaComprador(Parcel in) {
        this.idCalificaComprador = (Integer) in.readValue(Integer.class.getClassLoader());
        this.valor = (Integer) in.readValue(Integer.class.getClassLoader());
        this.comentario = in.readString();
        this.idMotorizadoPedido = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Creator<CalificaComprador> CREATOR = new Creator<CalificaComprador>() {
        @Override
        public CalificaComprador createFromParcel(Parcel source) {
            return new CalificaComprador(source);
        }

        @Override
        public CalificaComprador[] newArray(int size) {
            return new CalificaComprador[size];
        }
    };
}
