/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cobasoft.mad_vendedor.data.remote.model;


import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;

import com.google.gson.annotations.Expose;


public class FormaDePago implements Parcelable {

    @ColumnInfo(name = "ID_FORMA_DE_PAGO")
    @Expose
    private Integer idFormaDePago;
    @Expose
    private String nombre;

    public Integer getIdFormaDePago() {
        return idFormaDePago;
    }

    public void setIdFormaDePago(Integer idFormaDePago) {
        this.idFormaDePago = idFormaDePago;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.idFormaDePago);
        dest.writeString(this.nombre);
    }

    public FormaDePago() {
    }

    protected FormaDePago(Parcel in) {
        this.idFormaDePago = (Integer) in.readValue(Integer.class.getClassLoader());
        this.nombre = in.readString();
    }

    public static final Creator<FormaDePago> CREATOR = new Creator<FormaDePago>() {
        @Override
        public FormaDePago createFromParcel(Parcel source) {
            return new FormaDePago(source);
        }

        @Override
        public FormaDePago[] newArray(int size) {
            return new FormaDePago[size];
        }
    };
}
