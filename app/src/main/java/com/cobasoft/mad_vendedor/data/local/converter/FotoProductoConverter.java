package com.cobasoft.mad_vendedor.data.local.converter;

import androidx.room.TypeConverter;

import com.cobasoft.mad_vendedor.data.remote.model.Categoria;
import com.cobasoft.mad_vendedor.data.remote.model.FotoProducto;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class FotoProductoConverter {

    @TypeConverter
    public FotoProducto fromString(String value) {
        Type type= new TypeToken<Categoria>() {
        }.getType();
        FotoProducto fotoProducto = new Gson().fromJson(value, type);
        return fotoProducto;
    }

    @TypeConverter
    public String fromList(FotoProducto fotoProducto) {
        return new Gson().toJson(fotoProducto);
    }
}
