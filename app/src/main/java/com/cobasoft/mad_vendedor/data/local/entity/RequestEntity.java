package com.cobasoft.mad_vendedor.data.local.entity;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.Embedded;
import androidx.room.Entity;
import androidx.room.TypeConverters;

import com.cobasoft.mad_vendedor.data.local.converter.CabVentaConverter;
import com.cobasoft.mad_vendedor.data.local.converter.CalificaCompradorConverter;
import com.cobasoft.mad_vendedor.data.local.converter.CalificaMotorizadoConverter;
import com.cobasoft.mad_vendedor.data.local.converter.CalificaProductoConverter;
import com.cobasoft.mad_vendedor.data.local.converter.CategoriaConverter;
import com.cobasoft.mad_vendedor.data.local.converter.CompradorConverter;
import com.cobasoft.mad_vendedor.data.local.converter.DetVentaConverter;
import com.cobasoft.mad_vendedor.data.local.converter.EmpresaConverter;
import com.cobasoft.mad_vendedor.data.local.converter.FormaDePagoConverter;
import com.cobasoft.mad_vendedor.data.local.converter.FotoProductoConverter;
import com.cobasoft.mad_vendedor.data.local.converter.ImpuestoConverter;
import com.cobasoft.mad_vendedor.data.local.converter.MensajeCorreoConverter;
import com.cobasoft.mad_vendedor.data.local.converter.MotorizadoConverter;
import com.cobasoft.mad_vendedor.data.local.converter.MotorizadoPedidoConverter;
import com.cobasoft.mad_vendedor.data.local.converter.PedidoCompradorConverter;
import com.cobasoft.mad_vendedor.data.local.converter.PosicionMotorizadoConverter;
import com.cobasoft.mad_vendedor.data.local.converter.PregRespProductoConverter;
import com.cobasoft.mad_vendedor.data.local.converter.PreguntaConverter;
import com.cobasoft.mad_vendedor.data.local.converter.ProductoConverter;
import com.cobasoft.mad_vendedor.data.local.converter.RespuestaConverter;
import com.cobasoft.mad_vendedor.data.local.converter.StatusPedidoConverter;
import com.cobasoft.mad_vendedor.data.local.converter.SubcategoriaConverter;
import com.cobasoft.mad_vendedor.data.local.converter.VendedorConverter;
import com.cobasoft.mad_vendedor.data.local.converter.VendedorPedidoConverter;
import com.cobasoft.mad_vendedor.data.remote.model.CabVenta;
import com.cobasoft.mad_vendedor.data.remote.model.CalificaComprador;
import com.cobasoft.mad_vendedor.data.remote.model.CalificaMotorizado;
import com.cobasoft.mad_vendedor.data.remote.model.CalificaProducto;
import com.cobasoft.mad_vendedor.data.remote.model.Categoria;
import com.cobasoft.mad_vendedor.data.remote.model.Comprador;
import com.cobasoft.mad_vendedor.data.remote.model.DetVenta;
import com.cobasoft.mad_vendedor.data.remote.model.Empresa;
import com.cobasoft.mad_vendedor.data.remote.model.FormaDePago;
import com.cobasoft.mad_vendedor.data.remote.model.FotoProducto;
import com.cobasoft.mad_vendedor.data.remote.model.Impuesto;
import com.cobasoft.mad_vendedor.data.remote.model.MensajeCorreo;
import com.cobasoft.mad_vendedor.data.remote.model.Motorizado;
import com.cobasoft.mad_vendedor.data.remote.model.MotorizadoPedido;
import com.cobasoft.mad_vendedor.data.remote.model.PedidoComprador;
import com.cobasoft.mad_vendedor.data.remote.model.PosicionMotorizado;
import com.cobasoft.mad_vendedor.data.remote.model.PregRespProducto;
import com.cobasoft.mad_vendedor.data.remote.model.Pregunta;
import com.cobasoft.mad_vendedor.data.remote.model.Producto;
import com.cobasoft.mad_vendedor.data.remote.model.Respuesta;
import com.cobasoft.mad_vendedor.data.remote.model.StatusPedido;
import com.cobasoft.mad_vendedor.data.remote.model.Subcategoria;
import com.cobasoft.mad_vendedor.data.remote.model.Vendedor;
import com.cobasoft.mad_vendedor.data.remote.model.VendedorPedido;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity(primaryKeys = ("idLogVendedor"))
public class RequestEntity implements Parcelable {

    @SerializedName("id_log_vendedor")
    @Expose
    private Integer idLogVendedor;

    @Expose
    private String mensaje;

    @Expose
    private Integer estado;

    @Expose
    @TypeConverters(CabVentaConverter.class)
    @Embedded(prefix = "cabVenta_")
    private CabVenta cabVenta;
    @Expose
    @TypeConverters(CalificaCompradorConverter.class)
    @Embedded(prefix = "calificaComprador_")
    private CalificaComprador calificaComprador;
    @Expose
    @TypeConverters(CalificaMotorizadoConverter.class)
    @Embedded(prefix = "calificaMotorizado_")
    private CalificaMotorizado calificaMotorizado;
    @Expose
    @TypeConverters(CalificaProductoConverter.class)
    @Embedded(prefix = "calificaProducto_")
    private CalificaProducto calificaProducto;
    @Expose
    @TypeConverters(CategoriaConverter.class)
    @Embedded(prefix = "categoria_")
    private Categoria categoria;
    @Expose
    @TypeConverters(CompradorConverter.class)
    @Embedded(prefix = "comprador_")
    private Comprador comprador;
    @Expose
    @TypeConverters(DetVentaConverter.class)
    @Embedded(prefix = "detVenta_")
    private DetVenta detVenta;
    @Expose
    @TypeConverters(EmpresaConverter.class)
    @Embedded(prefix = "empresa_")
    private Empresa empresa;
    @Expose
    @TypeConverters(FormaDePagoConverter.class)
    @Embedded(prefix = "formaDePago_")
    private FormaDePago formaDePago;
    @Expose
    @TypeConverters(FotoProductoConverter.class)
    @Embedded(prefix = "fotoProducto_")
    private FotoProducto fotoProducto;
    @Expose
    @TypeConverters(ImpuestoConverter.class)
    @Embedded(prefix = "impuesto_")
    private Impuesto impuesto;
    @Expose
    @TypeConverters(MensajeCorreoConverter.class)
    @Embedded(prefix = "mensajeCorreo_")
    private MensajeCorreo mensajeCorreo;
    @Expose
    @TypeConverters(MotorizadoConverter.class)
    @Embedded(prefix = "motorizado_")
    private Motorizado motorizado;
    @Expose
    @TypeConverters(MotorizadoPedidoConverter.class)
    @Embedded(prefix = "motorizadoPedido_")
    private MotorizadoPedido motorizadoPedido;
    @Expose
    @TypeConverters(PedidoCompradorConverter.class)
    @Embedded(prefix = "pedidoComprador_")
    private PedidoComprador pedidoComprador;
    @Expose
    @TypeConverters(PosicionMotorizadoConverter.class)
    @Embedded(prefix = "posicionMotorizado_")
    private PosicionMotorizado posicionMotorizado;
    @Expose
    @TypeConverters(PregRespProductoConverter.class)
    @Embedded(prefix = "pregRespProducto_")
    private PregRespProducto pregRespProducto;
    @Expose
    @TypeConverters(PreguntaConverter.class)
    @Embedded(prefix = "pregunta_")
    private Pregunta pregunta;
    @Expose
    @TypeConverters(ProductoConverter.class)
    @Embedded(prefix = "producto_")
    private Producto producto;
    @Expose
    @TypeConverters(RespuestaConverter.class)
    @Embedded(prefix = "respuesta_")
    private Respuesta respuesta;
    @Expose
    @TypeConverters(StatusPedidoConverter.class)
    @Embedded(prefix = "statusPedido_")
    private StatusPedido statusPedido;
    @Expose
    @TypeConverters(SubcategoriaConverter.class)
    @Embedded(prefix = "subcategoria_")
    private Subcategoria subcategoria;
    @Expose
    @TypeConverters(VendedorConverter.class)
    @Embedded(prefix = "vendedor_")
    private Vendedor vendedor;
    @Expose
    @TypeConverters(VendedorPedidoConverter.class)
    @Embedded(prefix = "vendedorPedido_")
    private VendedorPedido vendedorPedido;


    public Integer getIdLogVendedor() {
        return idLogVendedor;
    }

    public void setIdLogVendedor(Integer idLogVendedor) {
        this.idLogVendedor = idLogVendedor;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public CabVenta getCabVenta() {
        return cabVenta;
    }

    public void setCabVenta(CabVenta cabVenta) {
        this.cabVenta = cabVenta;
    }

    public CalificaComprador getCalificaComprador() {
        return calificaComprador;
    }

    public void setCalificaComprador(CalificaComprador calificaComprador) {
        this.calificaComprador = calificaComprador;
    }

    public CalificaMotorizado getCalificaMotorizado() {
        return calificaMotorizado;
    }

    public void setCalificaMotorizado(CalificaMotorizado calificaMotorizado) {
        this.calificaMotorizado = calificaMotorizado;
    }

    public CalificaProducto getCalificaProducto() {
        return calificaProducto;
    }

    public void setCalificaProducto(CalificaProducto calificaProducto) {
        this.calificaProducto = calificaProducto;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public Comprador getComprador() {
        return comprador;
    }

    public void setComprador(Comprador comprador) {
        this.comprador = comprador;
    }

    public DetVenta getDetVenta() {
        return detVenta;
    }

    public void setDetVenta(DetVenta detVenta) {
        this.detVenta = detVenta;
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public FormaDePago getFormaDePago() {
        return formaDePago;
    }

    public void setFormaDePago(FormaDePago formaDePago) {
        this.formaDePago = formaDePago;
    }

    public FotoProducto getFotoProducto() {
        return fotoProducto;
    }

    public void setFotoProducto(FotoProducto fotoProducto) {
        this.fotoProducto = fotoProducto;
    }

    public Impuesto getImpuesto() {
        return impuesto;
    }

    public void setImpuesto(Impuesto impuesto) {
        this.impuesto = impuesto;
    }

    public MensajeCorreo getMensajeCorreo() {
        return mensajeCorreo;
    }

    public void setMensajeCorreo(MensajeCorreo mensajeCorreo) {
        this.mensajeCorreo = mensajeCorreo;
    }

    public Motorizado getMotorizado() {
        return motorizado;
    }

    public void setMotorizado(Motorizado motorizado) {
        this.motorizado = motorizado;
    }

    public MotorizadoPedido getMotorizadoPedido() {
        return motorizadoPedido;
    }

    public void setMotorizadoPedido(MotorizadoPedido motorizadoPedido) {
        this.motorizadoPedido = motorizadoPedido;
    }

    public PedidoComprador getPedidoComprador() {
        return pedidoComprador;
    }

    public void setPedidoComprador(PedidoComprador pedidoComprador) {
        this.pedidoComprador = pedidoComprador;
    }

    public PosicionMotorizado getPosicionMotorizado() {
        return posicionMotorizado;
    }

    public void setPosicionMotorizado(PosicionMotorizado posicionMotorizado) {
        this.posicionMotorizado = posicionMotorizado;
    }

    public PregRespProducto getPregRespProducto() {
        return pregRespProducto;
    }

    public void setPregRespProducto(PregRespProducto pregRespProducto) {
        this.pregRespProducto = pregRespProducto;
    }

    public Pregunta getPregunta() {
        return pregunta;
    }

    public void setPregunta(Pregunta pregunta) {
        this.pregunta = pregunta;
    }

    public Producto getProducto() {
        return producto;
    }

    public void setProducto(Producto producto) {
        this.producto = producto;
    }

    public Respuesta getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(Respuesta respuesta) {
        this.respuesta = respuesta;
    }

    public StatusPedido getStatusPedido() {
        return statusPedido;
    }

    public void setStatusPedido(StatusPedido statusPedido) {
        this.statusPedido = statusPedido;
    }

    public Subcategoria getSubcategoria() {
        return subcategoria;
    }

    public void setSubcategoria(Subcategoria subcategoria) {
        this.subcategoria = subcategoria;
    }

    public Vendedor getVendedor() {
        return vendedor;
    }

    public void setVendedor(Vendedor vendedor) {
        this.vendedor = vendedor;
    }

    public VendedorPedido getVendedorPedido() {
        return vendedorPedido;
    }

    public void setVendedorPedido(VendedorPedido vendedorPedido) {
        this.vendedorPedido = vendedorPedido;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.idLogVendedor);
        dest.writeString(this.mensaje);
        dest.writeValue(this.estado);
        dest.writeParcelable(this.cabVenta, flags);
        dest.writeParcelable(this.calificaComprador, flags);
        dest.writeParcelable(this.calificaMotorizado, flags);
        dest.writeParcelable(this.calificaProducto, flags);
        dest.writeParcelable(this.categoria, flags);
        dest.writeParcelable(this.comprador, flags);
        dest.writeParcelable(this.detVenta, flags);
        dest.writeParcelable(this.empresa, flags);
        dest.writeParcelable(this.formaDePago, flags);
        dest.writeParcelable(this.fotoProducto, flags);
        dest.writeParcelable(this.impuesto, flags);
        dest.writeParcelable(this.mensajeCorreo, flags);
        dest.writeParcelable(this.motorizado, flags);
        dest.writeParcelable(this.motorizadoPedido, flags);
        dest.writeParcelable(this.pedidoComprador, flags);
        dest.writeParcelable(this.posicionMotorizado, flags);
        dest.writeParcelable(this.pregRespProducto, flags);
        dest.writeParcelable(this.pregunta, flags);
        dest.writeParcelable(this.producto, flags);
        dest.writeParcelable(this.respuesta, flags);
        dest.writeParcelable(this.statusPedido, flags);
        dest.writeParcelable(this.subcategoria, flags);
        dest.writeParcelable(this.vendedor, flags);
        dest.writeParcelable(this.vendedorPedido, flags);
    }

    public RequestEntity() {
    }

    protected RequestEntity(Parcel in) {
        this.idLogVendedor = (Integer) in.readValue(Integer.class.getClassLoader());
        this.mensaje = in.readString();
        this.estado = (Integer) in.readValue(Integer.class.getClassLoader());
        this.cabVenta = in.readParcelable(CabVenta.class.getClassLoader());
        this.calificaComprador = in.readParcelable(CalificaComprador.class.getClassLoader());
        this.calificaMotorizado = in.readParcelable(CalificaMotorizado.class.getClassLoader());
        this.calificaProducto = in.readParcelable(CalificaProducto.class.getClassLoader());
        this.categoria = in.readParcelable(Categoria.class.getClassLoader());
        this.comprador = in.readParcelable(Comprador.class.getClassLoader());
        this.detVenta = in.readParcelable(DetVenta.class.getClassLoader());
        this.empresa = in.readParcelable(Empresa.class.getClassLoader());
        this.formaDePago = in.readParcelable(FormaDePago.class.getClassLoader());
        this.fotoProducto = in.readParcelable(FotoProducto.class.getClassLoader());
        this.impuesto = in.readParcelable(Impuesto.class.getClassLoader());
        this.mensajeCorreo = in.readParcelable(MensajeCorreo.class.getClassLoader());
        this.motorizado = in.readParcelable(Motorizado.class.getClassLoader());
        this.motorizadoPedido = in.readParcelable(MotorizadoPedido.class.getClassLoader());
        this.pedidoComprador = in.readParcelable(PedidoComprador.class.getClassLoader());
        this.posicionMotorizado = in.readParcelable(PosicionMotorizado.class.getClassLoader());
        this.pregRespProducto = in.readParcelable(PregRespProducto.class.getClassLoader());
        this.pregunta = in.readParcelable(Pregunta.class.getClassLoader());
        this.producto = in.readParcelable(Producto.class.getClassLoader());
        this.respuesta = in.readParcelable(Respuesta.class.getClassLoader());
        this.statusPedido = in.readParcelable(StatusPedido.class.getClassLoader());
        this.subcategoria = in.readParcelable(Subcategoria.class.getClassLoader());
        this.vendedor = in.readParcelable(Vendedor.class.getClassLoader());
        this.vendedorPedido = in.readParcelable(VendedorPedido.class.getClassLoader());
    }

    public static final Creator<RequestEntity> CREATOR = new Creator<RequestEntity>() {
        @Override
        public RequestEntity createFromParcel(Parcel source) {
            return new RequestEntity(source);
        }

        @Override
        public RequestEntity[] newArray(int size) {
            return new RequestEntity[size];
        }
    };
}
