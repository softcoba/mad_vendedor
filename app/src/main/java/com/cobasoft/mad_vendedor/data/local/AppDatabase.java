package com.cobasoft.mad_vendedor.data.local;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import com.cobasoft.mad_vendedor.data.local.converter.CabVentaConverter;
import com.cobasoft.mad_vendedor.data.local.converter.CalificaCompradorConverter;
import com.cobasoft.mad_vendedor.data.local.converter.CalificaMotorizadoConverter;
import com.cobasoft.mad_vendedor.data.local.converter.CalificaProductoConverter;
import com.cobasoft.mad_vendedor.data.local.converter.CategoriaConverter;
import com.cobasoft.mad_vendedor.data.local.converter.CompradorConverter;
import com.cobasoft.mad_vendedor.data.local.converter.DateTypeConverter;
import com.cobasoft.mad_vendedor.data.local.converter.DetVentaConverter;
import com.cobasoft.mad_vendedor.data.local.converter.EmpresaConverter;
import com.cobasoft.mad_vendedor.data.local.converter.FormaDePagoConverter;
import com.cobasoft.mad_vendedor.data.local.converter.FotoProductoConverter;
import com.cobasoft.mad_vendedor.data.local.converter.ImpuestoConverter;
import com.cobasoft.mad_vendedor.data.local.converter.MensajeCorreoConverter;
import com.cobasoft.mad_vendedor.data.local.converter.MotorizadoConverter;
import com.cobasoft.mad_vendedor.data.local.converter.MotorizadoPedidoConverter;
import com.cobasoft.mad_vendedor.data.local.converter.PedidoCompradorConverter;
import com.cobasoft.mad_vendedor.data.local.converter.PosicionMotorizadoConverter;
import com.cobasoft.mad_vendedor.data.local.converter.PregRespProductoConverter;
import com.cobasoft.mad_vendedor.data.local.converter.PreguntaConverter;
import com.cobasoft.mad_vendedor.data.local.converter.ProductoConverter;
import com.cobasoft.mad_vendedor.data.local.converter.RespuestaConverter;
import com.cobasoft.mad_vendedor.data.local.converter.StatusPedidoConverter;
import com.cobasoft.mad_vendedor.data.local.converter.SubcategoriaConverter;
import com.cobasoft.mad_vendedor.data.local.converter.VendedorConverter;
import com.cobasoft.mad_vendedor.data.local.converter.VendedorPedidoConverter;
import com.cobasoft.mad_vendedor.data.local.dao.SesionDao;
import com.cobasoft.mad_vendedor.data.local.entity.RequestEntity;

import static com.cobasoft.mad_vendedor.AppConstants.ROOM_DATABASE;

@Database(entities = {
        RequestEntity.class},
        version = 1,
        exportSchema = false)
@TypeConverters({CabVentaConverter.class,
        CalificaCompradorConverter.class,
        CalificaMotorizadoConverter.class,
        CalificaProductoConverter.class,
        CategoriaConverter.class,
        CompradorConverter.class,
        DateTypeConverter.class,
        DetVentaConverter.class,
        EmpresaConverter.class,
        FormaDePagoConverter.class,
        FotoProductoConverter.class,
        ImpuestoConverter.class,
        MensajeCorreoConverter.class,
        MotorizadoConverter.class,
        MotorizadoPedidoConverter.class,
        PedidoCompradorConverter.class,
        PosicionMotorizadoConverter.class,
        PregRespProductoConverter.class,
        PreguntaConverter.class,
        ProductoConverter.class,
        RespuestaConverter.class,
        StatusPedidoConverter.class,
        SubcategoriaConverter.class,
        VendedorConverter.class,
        VendedorPedidoConverter.class})
public abstract class AppDatabase extends RoomDatabase {

    public abstract SesionDao sesionDao();

    private static volatile AppDatabase INSTANCE;

    public static AppDatabase getInstance(Context context) {
        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = buildDatabase(context);
                }
            }
        }
        return INSTANCE;
    }

    private static AppDatabase buildDatabase(Context context) {
        return Room.databaseBuilder(context, AppDatabase.class, ROOM_DATABASE).allowMainThreadQueries().build();
    }
}
