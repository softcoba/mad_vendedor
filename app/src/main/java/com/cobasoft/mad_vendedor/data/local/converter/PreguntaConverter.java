package com.cobasoft.mad_vendedor.data.local.converter;

import androidx.room.TypeConverter;

import com.cobasoft.mad_vendedor.data.remote.model.Categoria;
import com.cobasoft.mad_vendedor.data.remote.model.Pregunta;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class PreguntaConverter {

    @TypeConverter
    public Pregunta fromString(String value) {
        Type type= new TypeToken<Categoria>() {
        }.getType();
        Pregunta pregunta = new Gson().fromJson(value, type);
        return pregunta;
    }

    @TypeConverter
    public String fromList(Pregunta pregunta) {
        return new Gson().toJson(pregunta);
    }
}
