/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cobasoft.mad_vendedor.data.remote.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;

import com.google.gson.annotations.Expose;


public class CalificaMotorizado implements Parcelable {

    @ColumnInfo(name = "ID_CALIFICA_MOTORIZADO")
    @Expose
    private Integer idCalificaMotorizado;
    @Expose
    private Integer valor;
    @Expose
    private String comentario;
    @ColumnInfo(name = "ID_PEDIDO_COMPRADOR")
    @Expose
    private Integer idPedidoComprador;

    public Integer getIdCalificaMotorizado() {
        return idCalificaMotorizado;
    }

    public void setIdCalificaMotorizado(Integer idCalificaMotorizado) {
        this.idCalificaMotorizado = idCalificaMotorizado;
    }

    public Integer getValor() {
        return valor;
    }

    public void setValor(Integer valor) {
        this.valor = valor;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public Integer getIdPedidoComprador() {
        return idPedidoComprador;
    }

    public void setIdPedidoComprador(Integer idPedidoComprador) {
        this.idPedidoComprador = idPedidoComprador;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.idCalificaMotorizado);
        dest.writeValue(this.valor);
        dest.writeString(this.comentario);
        dest.writeValue(this.idPedidoComprador);
    }

    public CalificaMotorizado() {
    }

    protected CalificaMotorizado(Parcel in) {
        this.idCalificaMotorizado = (Integer) in.readValue(Integer.class.getClassLoader());
        this.valor = (Integer) in.readValue(Integer.class.getClassLoader());
        this.comentario = in.readString();
        this.idPedidoComprador = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Creator<CalificaMotorizado> CREATOR = new Creator<CalificaMotorizado>() {
        @Override
        public CalificaMotorizado createFromParcel(Parcel source) {
            return new CalificaMotorizado(source);
        }

        @Override
        public CalificaMotorizado[] newArray(int size) {
            return new CalificaMotorizado[size];
        }
    };
}
