/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cobasoft.mad_vendedor.data.remote.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;

import com.google.gson.annotations.Expose;


public class Empresa implements Parcelable {

    @ColumnInfo(name = "ID_EMPRESA")
    @Expose
    private Integer idEmpresa;
    @ColumnInfo(name = "RAZON_SOCIAL")
    @Expose
    private String razonSocial;
    @Expose
    private String estado;
    @Expose
    private String correo;
    @Expose
    private Integer ruc;
    @ColumnInfo(name = "LINK_POLITICA_PRIVACIDAD")
    @Expose
    private String linkPoliticaPrivacidad;
    @Expose
    private String celular;

    public Integer getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(Integer idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getRazonSocial() {
        return razonSocial;
    }

    public void setRazonSocial(String razonSocial) {
        this.razonSocial = razonSocial;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public Integer getRuc() {
        return ruc;
    }

    public void setRuc(Integer ruc) {
        this.ruc = ruc;
    }

    public String getLinkPoliticaPrivacidad() {
        return linkPoliticaPrivacidad;
    }

    public void setLinkPoliticaPrivacidad(String linkPoliticaPrivacidad) {
        this.linkPoliticaPrivacidad = linkPoliticaPrivacidad;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.idEmpresa);
        dest.writeString(this.razonSocial);
        dest.writeString(this.estado);
        dest.writeString(this.correo);
        dest.writeValue(this.ruc);
        dest.writeString(this.linkPoliticaPrivacidad);
        dest.writeString(this.celular);
    }

    public Empresa() {
    }

    protected Empresa(Parcel in) {
        this.idEmpresa = (Integer) in.readValue(Integer.class.getClassLoader());
        this.razonSocial = in.readString();
        this.estado = in.readString();
        this.correo = in.readString();
        this.ruc = (Integer) in.readValue(Integer.class.getClassLoader());
        this.linkPoliticaPrivacidad = in.readString();
        this.celular = in.readString();
    }

    public static final Creator<Empresa> CREATOR = new Creator<Empresa>() {
        @Override
        public Empresa createFromParcel(Parcel source) {
            return new Empresa(source);
        }

        @Override
        public Empresa[] newArray(int size) {
            return new Empresa[size];
        }
    };
}
