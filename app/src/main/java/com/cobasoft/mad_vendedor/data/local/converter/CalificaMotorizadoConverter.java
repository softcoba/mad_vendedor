package com.cobasoft.mad_vendedor.data.local.converter;

import androidx.room.TypeConverter;

import com.cobasoft.mad_vendedor.data.remote.model.CalificaMotorizado;
import com.cobasoft.mad_vendedor.data.remote.model.Categoria;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class CalificaMotorizadoConverter {

    @TypeConverter
    public CalificaMotorizado fromString(String value) {
        Type type= new TypeToken<Categoria>() {
        }.getType();
        CalificaMotorizado calificaMotorizado = new Gson().fromJson(value, type);
        return calificaMotorizado;
    }

    @TypeConverter
    public String fromList(CalificaMotorizado calificaMotorizado) {
        return new Gson().toJson(calificaMotorizado);
    }
}
