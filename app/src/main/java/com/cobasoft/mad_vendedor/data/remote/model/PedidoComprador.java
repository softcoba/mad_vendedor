/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cobasoft.mad_vendedor.data.remote.model;


import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.TypeConverters;

import com.cobasoft.mad_vendedor.data.local.converter.DateTypeConverter;
import com.google.gson.annotations.Expose;

import java.util.Date;


public class PedidoComprador implements Parcelable {

    @ColumnInfo(name = "ID_PEDIDO_COMPRADOR")
    @Expose
    private Integer idPedidoComprador;
    @Expose
    private Double latitud;
    @Expose
    private Double longitud;
    @ColumnInfo(name = "FECHA_HORA_REGISTRO")
    @Expose
    @TypeConverters(DateTypeConverter.class)
    private Date fechaHoraRegistro;
    @Expose
    private String direccion;
    @Expose
    private String ciudad;
    @ColumnInfo(name = "STATUS_PEDIDO")
    @Expose
    private String statusPedido;
    @Expose
    private String observacion;
    @ColumnInfo(name = "ID_COMPRADOR")
    @Expose
    private Integer idComprador;

    public Integer getIdPedidoComprador() {
        return idPedidoComprador;
    }

    public void setIdPedidoComprador(Integer idPedidoComprador) {
        this.idPedidoComprador = idPedidoComprador;
    }

    public Double getLatitud() {
        return latitud;
    }

    public void setLatitud(Double latitud) {
        this.latitud = latitud;
    }

    public Double getLongitud() {
        return longitud;
    }

    public void setLongitud(Double longitud) {
        this.longitud = longitud;
    }

    public Date getFechaHoraRegistro() {
        return fechaHoraRegistro;
    }

    public void setFechaHoraRegistro(Date fechaHoraRegistro) {
        this.fechaHoraRegistro = fechaHoraRegistro;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getStatusPedido() {
        return statusPedido;
    }

    public void setStatusPedido(String statusPedido) {
        this.statusPedido = statusPedido;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public Integer getIdComprador() {
        return idComprador;
    }

    public void setIdComprador(Integer idComprador) {
        this.idComprador = idComprador;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.idPedidoComprador);
        dest.writeValue(this.latitud);
        dest.writeValue(this.longitud);
        dest.writeLong(this.fechaHoraRegistro != null ? this.fechaHoraRegistro.getTime() : -1);
        dest.writeString(this.direccion);
        dest.writeString(this.ciudad);
        dest.writeString(this.statusPedido);
        dest.writeString(this.observacion);
        dest.writeValue(this.idComprador);
    }

    public PedidoComprador() {
    }

    protected PedidoComprador(Parcel in) {
        this.idPedidoComprador = (Integer) in.readValue(Integer.class.getClassLoader());
        this.latitud = (Double) in.readValue(Double.class.getClassLoader());
        this.longitud = (Double) in.readValue(Double.class.getClassLoader());
        long tmpFechaHoraRegistro = in.readLong();
        this.fechaHoraRegistro = tmpFechaHoraRegistro == -1 ? null : new Date(tmpFechaHoraRegistro);
        this.direccion = in.readString();
        this.ciudad = in.readString();
        this.statusPedido = in.readString();
        this.observacion = in.readString();
        this.idComprador = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Parcelable.Creator<PedidoComprador> CREATOR = new Parcelable.Creator<PedidoComprador>() {
        @Override
        public PedidoComprador createFromParcel(Parcel source) {
            return new PedidoComprador(source);
        }

        @Override
        public PedidoComprador[] newArray(int size) {
            return new PedidoComprador[size];
        }
    };
}
