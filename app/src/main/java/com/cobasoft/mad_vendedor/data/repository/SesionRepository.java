package com.cobasoft.mad_vendedor.data.repository;

import androidx.annotation.NonNull;

import com.cobasoft.mad_vendedor.data.local.dao.SesionDao;
import com.cobasoft.mad_vendedor.data.local.entity.RequestEntity;
import com.cobasoft.mad_vendedor.data.remote.api.ApiService;
import com.cobasoft.mad_vendedor.data.remote.model.BaseDeDatosResponse;
import com.cobasoft.mad_vendedor.data.resource.NetworkBoundResource;
import com.cobasoft.mad_vendedor.data.resource.Resource;

import javax.inject.Singleton;

import io.reactivex.Flowable;
import io.reactivex.Observable;


@Singleton
public class SesionRepository {

    private ApiService apiService;
    private SesionDao sesionDao;

    public SesionRepository(SesionDao sesionDao, ApiService apiService) {
        this.sesionDao = sesionDao;
        this.apiService = apiService;
    }

    public Observable<Resource<RequestEntity>> crearActualizarVendedor(RequestEntity requestEntity) {
        return new NetworkBoundResource<RequestEntity, BaseDeDatosResponse>() {

            @Override // este es el 1er metodo que se dispara
            protected boolean shouldFetch() {
                return true;
            }

            @NonNull // este es el 2do metodo que se dispara
            @Override// aqui se obtiene datos del servidor
            protected Observable<Resource<BaseDeDatosResponse>> createCall() {

                System.out.println("aqui va createCall ");
                return apiService.crearActualizarVendedor(requestEntity)
                        .flatMap(baseDeDatosResponse -> Observable.just(
                                baseDeDatosResponse == null ? // aqui se guarda el estado de la red
                                        Resource.error("error mad", new BaseDeDatosResponse()) :
                                        Resource.success(baseDeDatosResponse)));
            }

            @NonNull // este es el 3er metodo que se dispara
            @Override // este metodo lo utilizamos para invocar los datos de ROOM
            protected Flowable<RequestEntity> loadFromDb() {

                System.out.println("aqui va loadFromDb ");
                RequestEntity baseDeDatosEntity = sesionDao.obtenerVendedorByCorreo(requestEntity.getVendedor().getCorreo());
                if (baseDeDatosEntity == null) {
                    System.out.println("aqui va Flowable.empty() ");
                    // cuando se retorna vacio, el Observable no emite ningun valor
                    // es decir el observer no escucha nada, no llega ni error ni nada
                    return Flowable.empty();
                } else
                    System.out.println("No Flowable " + baseDeDatosEntity);
                return Flowable.just(baseDeDatosEntity);
            }

            // este es el 4to metodo que se dispara
            @Override// aqui se guarda los datos de api en la base local
            protected void saveCallResult(@NonNull BaseDeDatosResponse baseDeDatosResponse) {
                System.out.println("aqui va saveCallResult item " + baseDeDatosResponse);

                RequestEntity baseDeDatosEntity = new RequestEntity();

                sesionDao.insertarBaseDeDatos(baseDeDatosEntity);

            }

        }.getAsObservable();
    }

    public Observable<Resource<RequestEntity>> loginVendedor(RequestEntity requestEntity) {
        return new NetworkBoundResource<RequestEntity, BaseDeDatosResponse>() {

            @Override // este es el 1er metodo que se dispara
            protected boolean shouldFetch() {
                return true;
            }

            @NonNull // este es el 2do metodo que se dispara
            @Override// aqui se obtiene datos del servidor
            protected Observable<Resource<BaseDeDatosResponse>> createCall() {

                System.out.println("aqui va createCall ");
                return apiService.login(requestEntity)
                        .flatMap(baseDeDatosResponse -> Observable.just(
                                baseDeDatosResponse == null ? // aqui se guarda el estado de la red
                                        Resource.error("error mad", new BaseDeDatosResponse()) :
                                        Resource.success(baseDeDatosResponse)));
            }

            @NonNull // este es el 3er metodo que se dispara
            @Override // este metodo lo utilizamos para invocar los datos de ROOM
            protected Flowable<RequestEntity> loadFromDb() {

                System.out.println("aqui va loadFromDb ");
                RequestEntity baseDeDatosEntity = sesionDao.obtenerVendedorByCorreo(requestEntity.getVendedor().getCorreo());
                if (baseDeDatosEntity == null) {
                    System.out.println("aqui va Flowable.empty() ");
                    // cuando se retorna vacio, el Observable no emite ningun valor
                    // es decir el observer no escucha nada, no llega ni error ni nada
                    return Flowable.empty();
                } else
                    System.out.println("No Flowable " + baseDeDatosEntity);
                return Flowable.just(baseDeDatosEntity);
            }

            // este es el 4to metodo que se dispara
            @Override// aqui se guarda los datos de api en la base local
            protected void saveCallResult(@NonNull BaseDeDatosResponse baseDeDatosResponse) {
                System.out.println("aqui va saveCallResult item " + baseDeDatosResponse);

                RequestEntity baseDeDatosEntity = new RequestEntity();

                baseDeDatosEntity.setVendedor(baseDeDatosResponse.getVendedor());

                sesionDao.insertarBaseDeDatos(baseDeDatosEntity);
            }

        }.getAsObservable();
    }

}
