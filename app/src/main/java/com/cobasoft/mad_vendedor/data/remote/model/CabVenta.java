/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cobasoft.mad_vendedor.data.remote.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.TypeConverters;

import com.cobasoft.mad_vendedor.data.local.converter.DateTypeConverter;
import com.google.gson.annotations.Expose;

import java.util.Date;

public class CabVenta implements Parcelable {

    @ColumnInfo(name = "ID_CAB_VENTA")
    @Expose
    private Integer idCabVenta;
    @Expose
    @TypeConverters(DateTypeConverter.class)
    private Date fecha;
    @Expose
    private Integer descuento;
    @ColumnInfo(name = "SUBTOTAL_VENTA")
    @Expose
    private Double subtotalVenta;
    @ColumnInfo(name = "SUBTOTAL_DESCUENTO")
    @Expose
    private Double subtotalDescuento;
    @Expose
    private Double total;
    @Expose
    private Double iva;
    @ColumnInfo(name = "ID_PEDIDO_COMPRADOR")
    @Expose
    private Integer idPedidoComprador;

    public Integer getIdCabVenta() {
        return idCabVenta;
    }

    public void setIdCabVenta(Integer idCabVenta) {
        this.idCabVenta = idCabVenta;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Integer getDescuento() {
        return descuento;
    }

    public void setDescuento(Integer descuento) {
        this.descuento = descuento;
    }

    public Double getSubtotalVenta() {
        return subtotalVenta;
    }

    public void setSubtotalVenta(Double subtotalVenta) {
        this.subtotalVenta = subtotalVenta;
    }

    public Double getSubtotalDescuento() {
        return subtotalDescuento;
    }

    public void setSubtotalDescuento(Double subtotalDescuento) {
        this.subtotalDescuento = subtotalDescuento;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Double getIva() {
        return iva;
    }

    public void setIva(Double iva) {
        this.iva = iva;
    }

    public Integer getIdPedidoComprador() {
        return idPedidoComprador;
    }

    public void setIdPedidoComprador(Integer idPedidoComprador) {
        this.idPedidoComprador = idPedidoComprador;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.idCabVenta);
        dest.writeLong(this.fecha != null ? this.fecha.getTime() : -1);
        dest.writeValue(this.descuento);
        dest.writeValue(this.subtotalVenta);
        dest.writeValue(this.subtotalDescuento);
        dest.writeValue(this.total);
        dest.writeValue(this.iva);
        dest.writeValue(this.idPedidoComprador);
    }

    public CabVenta() {
    }

    protected CabVenta(Parcel in) {
        this.idCabVenta = (Integer) in.readValue(Integer.class.getClassLoader());
        long tmpFecha = in.readLong();
        this.fecha = tmpFecha == -1 ? null : new Date(tmpFecha);
        this.descuento = (Integer) in.readValue(Integer.class.getClassLoader());
        this.subtotalVenta = (Double) in.readValue(Double.class.getClassLoader());
        this.subtotalDescuento = (Double) in.readValue(Double.class.getClassLoader());
        this.total = (Double) in.readValue(Double.class.getClassLoader());
        this.iva = (Double) in.readValue(Double.class.getClassLoader());
        this.idPedidoComprador = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Creator<CabVenta> CREATOR = new Creator<CabVenta>() {
        @Override
        public CabVenta createFromParcel(Parcel source) {
            return new CabVenta(source);
        }

        @Override
        public CabVenta[] newArray(int size) {
            return new CabVenta[size];
        }
    };
}
