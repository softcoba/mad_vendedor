/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cobasoft.mad_vendedor.data.remote.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;

import com.google.gson.annotations.Expose;

public class DetVenta implements Parcelable {

    @ColumnInfo(name = "ID_DET_VENTA")
    @Expose
    private Integer idDetVenta;
    @Expose
    private Integer cantidad;
    @ColumnInfo(name = "PRECIO_TOTAL")
    @Expose
    private Double precioTotal;
    @ColumnInfo(name = "ID_PRODUCTO")
    @Expose
    private Integer idProducto;
    @ColumnInfo(name = "ID_CAB_VENTA")
    @Expose
    private Integer idCabVenta;

    public Integer getIdDetVenta() {
        return idDetVenta;
    }

    public void setIdDetVenta(Integer idDetVenta) {
        this.idDetVenta = idDetVenta;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }

    public Double getPrecioTotal() {
        return precioTotal;
    }

    public void setPrecioTotal(Double precioTotal) {
        this.precioTotal = precioTotal;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdCabVenta() {
        return idCabVenta;
    }

    public void setIdCabVenta(Integer idCabVenta) {
        this.idCabVenta = idCabVenta;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.idDetVenta);
        dest.writeValue(this.cantidad);
        dest.writeValue(this.precioTotal);
        dest.writeValue(this.idProducto);
        dest.writeValue(this.idCabVenta);
    }

    public DetVenta() {
    }

    protected DetVenta(Parcel in) {
        this.idDetVenta = (Integer) in.readValue(Integer.class.getClassLoader());
        this.cantidad = (Integer) in.readValue(Integer.class.getClassLoader());
        this.precioTotal = (Double) in.readValue(Double.class.getClassLoader());
        this.idProducto = (Integer) in.readValue(Integer.class.getClassLoader());
        this.idCabVenta = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Creator<DetVenta> CREATOR = new Creator<DetVenta>() {
        @Override
        public DetVenta createFromParcel(Parcel source) {
            return new DetVenta(source);
        }

        @Override
        public DetVenta[] newArray(int size) {
            return new DetVenta[size];
        }
    };
}
