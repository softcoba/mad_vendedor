/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cobasoft.mad_vendedor.data.remote.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;

import com.google.gson.annotations.Expose;


public class PregRespProducto implements Parcelable {

    @ColumnInfo(name = "ID_PREG_RESP_PRODUCTO")
    @Expose
    private Integer idPregRespProducto;
    @ColumnInfo(name = "ID_RESPUESTA")
    private Integer idRespuesta;
    @ColumnInfo(name = "ID_PRODUCTO")
    @Expose
    private Integer idProducto;
    @ColumnInfo(name = "ID_PREGUNTA")
    @Expose
    private Integer idPregunta;

    public Integer getIdPregRespProducto() {
        return idPregRespProducto;
    }

    public void setIdPregRespProducto(Integer idPregRespProducto) {
        this.idPregRespProducto = idPregRespProducto;
    }

    public Integer getIdRespuesta() {
        return idRespuesta;
    }

    public void setIdRespuesta(Integer idRespuesta) {
        this.idRespuesta = idRespuesta;
    }

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(Integer idProducto) {
        this.idProducto = idProducto;
    }

    public Integer getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(Integer idPregunta) {
        this.idPregunta = idPregunta;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.idPregRespProducto);
        dest.writeValue(this.idRespuesta);
        dest.writeValue(this.idProducto);
        dest.writeValue(this.idPregunta);
    }

    public PregRespProducto() {
    }

    protected PregRespProducto(Parcel in) {
        this.idPregRespProducto = (Integer) in.readValue(Integer.class.getClassLoader());
        this.idRespuesta = (Integer) in.readValue(Integer.class.getClassLoader());
        this.idProducto = (Integer) in.readValue(Integer.class.getClassLoader());
        this.idPregunta = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Parcelable.Creator<PregRespProducto> CREATOR = new Parcelable.Creator<PregRespProducto>() {
        @Override
        public PregRespProducto createFromParcel(Parcel source) {
            return new PregRespProducto(source);
        }

        @Override
        public PregRespProducto[] newArray(int size) {
            return new PregRespProducto[size];
        }
    };
}
