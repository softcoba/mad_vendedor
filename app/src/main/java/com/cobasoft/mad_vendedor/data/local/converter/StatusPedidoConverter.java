package com.cobasoft.mad_vendedor.data.local.converter;

import androidx.room.TypeConverter;

import com.cobasoft.mad_vendedor.data.remote.model.Categoria;
import com.cobasoft.mad_vendedor.data.remote.model.StatusPedido;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class StatusPedidoConverter {

    @TypeConverter
    public StatusPedido fromString(String value) {
        Type type= new TypeToken<Categoria>() {
        }.getType();
        StatusPedido statusPedido = new Gson().fromJson(value, type);
        return statusPedido;
    }

    @TypeConverter
    public String fromList(StatusPedido statusPedido) {
        return new Gson().toJson(statusPedido);
    }
}
