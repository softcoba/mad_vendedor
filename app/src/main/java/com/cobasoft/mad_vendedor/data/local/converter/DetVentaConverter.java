package com.cobasoft.mad_vendedor.data.local.converter;

import androidx.room.TypeConverter;

import com.cobasoft.mad_vendedor.data.remote.model.Categoria;
import com.cobasoft.mad_vendedor.data.remote.model.DetVenta;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class DetVentaConverter {

    @TypeConverter
    public DetVenta fromString(String value) {
        Type type= new TypeToken<Categoria>() {
        }.getType();
        DetVenta detVenta = new Gson().fromJson(value, type);
        return detVenta;
    }

    @TypeConverter
    public String fromList(DetVenta detVenta) {
        return new Gson().toJson(detVenta);
    }
}
