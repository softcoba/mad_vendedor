package com.cobasoft.mad_vendedor.data.local.converter;

import androidx.room.TypeConverter;

import com.cobasoft.mad_vendedor.data.remote.model.Categoria;
import com.cobasoft.mad_vendedor.data.remote.model.VendedorPedido;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class VendedorPedidoConverter {

    @TypeConverter
    public VendedorPedido fromString(String value) {
        Type type= new TypeToken<Categoria>() {
        }.getType();
        VendedorPedido vendedorPedido = new Gson().fromJson(value, type);
        return vendedorPedido;
    }

    @TypeConverter
    public String fromList(VendedorPedido vendedorPedido) {
        return new Gson().toJson(vendedorPedido);
    }
}
