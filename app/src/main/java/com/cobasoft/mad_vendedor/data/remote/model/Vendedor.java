/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cobasoft.mad_vendedor.data.remote.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.TypeConverters;

import com.cobasoft.mad_vendedor.data.local.converter.VendedorPedidoListConverter;
import com.google.gson.annotations.Expose;

import java.util.List;


public class Vendedor implements Parcelable {

    @ColumnInfo(name = "ID_VENDEDOR")
    @Expose
    private Integer idVendedor;
    @Expose
    private String nombre;
    @Expose
    private String apellido;
    @Expose
    private String celular;
    @Expose
    private String cedula;
    @Expose
    private String correo;
    @Expose
    private String password;
    @Expose
    @ColumnInfo(name = "NUM_INTENTOS")
    private Integer numIntentos;
    @ColumnInfo(name = "FECHA_REGISTRO")
    @Expose
//    @TypeConverters(DateTypeConverter.class)
    private String fechaRegistro;

    @Expose
    private String estado;

    @Expose
    @TypeConverters(VendedorPedidoListConverter.class)
    private List<VendedorPedido> vendedorPedidoList;


    public Integer getIdVendedor() {
        return idVendedor;
    }

    public void setIdVendedor(Integer idVendedor) {
        this.idVendedor = idVendedor;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(String fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Integer getNumIntentos() {
        return numIntentos;
    }

    public void setNumIntentos(Integer numIntentos) {
        this.numIntentos = numIntentos;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<VendedorPedido> getVendedorPedidoList() {
        return vendedorPedidoList;
    }

    public void setVendedorPedidoList(List<VendedorPedido> vendedorPedidoList) {
        this.vendedorPedidoList = vendedorPedidoList;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.idVendedor);
        dest.writeString(this.nombre);
        dest.writeString(this.apellido);
        dest.writeString(this.celular);
        dest.writeString(this.cedula);
        dest.writeString(this.correo);
        dest.writeString(this.password);
        dest.writeValue(this.numIntentos);
        dest.writeString(this.fechaRegistro);
        dest.writeString(this.estado);
        dest.writeTypedList(this.vendedorPedidoList);
    }

    public Vendedor() {
    }

    protected Vendedor(Parcel in) {
        this.idVendedor = (Integer) in.readValue(Integer.class.getClassLoader());
        this.nombre = in.readString();
        this.apellido = in.readString();
        this.celular = in.readString();
        this.cedula = in.readString();
        this.correo = in.readString();
        this.password = in.readString();
        this.numIntentos = (Integer) in.readValue(Integer.class.getClassLoader());
        this.fechaRegistro = in.readString();
        this.estado = in.readString();
        this.vendedorPedidoList = in.createTypedArrayList(VendedorPedido.CREATOR);
    }

    public static final Creator<Vendedor> CREATOR = new Creator<Vendedor>() {
        @Override
        public Vendedor createFromParcel(Parcel source) {
            return new Vendedor(source);
        }

        @Override
        public Vendedor[] newArray(int size) {
            return new Vendedor[size];
        }
    };
}
