package com.cobasoft.mad_vendedor.data.resource;

public enum Status {
    SUCCESS,
    ERROR,
    LOADING
}
