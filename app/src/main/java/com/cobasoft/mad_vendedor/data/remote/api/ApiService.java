package com.cobasoft.mad_vendedor.data.remote.api;

import com.cobasoft.mad_vendedor.data.local.entity.RequestEntity;
import com.cobasoft.mad_vendedor.data.remote.model.BaseDeDatosResponse;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;


public interface ApiService {

    @POST("v1/crearActualizarVendedor?")
    Observable<BaseDeDatosResponse> crearActualizarVendedor(@Body RequestEntity requestEntity);


    @POST("v1/loginVendedor?")
    Observable<BaseDeDatosResponse> login(@Body RequestEntity requestEntity);

    //para probar er servicio
    @GET("v1/factorial?")
    Call<String> factorial(@Query("base") String genrci_descripcion);


}
