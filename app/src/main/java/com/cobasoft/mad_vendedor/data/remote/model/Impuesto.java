/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cobasoft.mad_vendedor.data.remote.model;


import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.TypeConverters;

import com.cobasoft.mad_vendedor.data.local.converter.DateTypeConverter;
import com.google.gson.annotations.Expose;

import java.util.Date;


public class Impuesto implements Parcelable {

    @ColumnInfo(name = "ID_IMPUESTO")
    @Expose
    private Integer idImpuesto;
    @Expose
    private String nombre;
    @Expose
    private Integer porcentaje;
    @ColumnInfo(name = "FECHA_EXPIRACION")
    @Expose
    @TypeConverters(DateTypeConverter.class)
    private Date fechaExpiracion;
    @ColumnInfo(name = "FECHA_REGISTRO")
    @Expose
    @TypeConverters(DateTypeConverter.class)
    private Date fechaRegistro;

    public Integer getIdImpuesto() {
        return idImpuesto;
    }

    public void setIdImpuesto(Integer idImpuesto) {
        this.idImpuesto = idImpuesto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(Integer porcentaje) {
        this.porcentaje = porcentaje;
    }

    public Date getFechaExpiracion() {
        return fechaExpiracion;
    }

    public void setFechaExpiracion(Date fechaExpiracion) {
        this.fechaExpiracion = fechaExpiracion;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.idImpuesto);
        dest.writeString(this.nombre);
        dest.writeValue(this.porcentaje);
        dest.writeLong(this.fechaExpiracion != null ? this.fechaExpiracion.getTime() : -1);
        dest.writeLong(this.fechaRegistro != null ? this.fechaRegistro.getTime() : -1);
    }

    public Impuesto() {
    }

    protected Impuesto(Parcel in) {
        this.idImpuesto = (Integer) in.readValue(Integer.class.getClassLoader());
        this.nombre = in.readString();
        this.porcentaje = (Integer) in.readValue(Integer.class.getClassLoader());
        long tmpFechaExpiracion = in.readLong();
        this.fechaExpiracion = tmpFechaExpiracion == -1 ? null : new Date(tmpFechaExpiracion);
        long tmpFechaRegistro = in.readLong();
        this.fechaRegistro = tmpFechaRegistro == -1 ? null : new Date(tmpFechaRegistro);
    }

    public static final Creator<Impuesto> CREATOR = new Creator<Impuesto>() {
        @Override
        public Impuesto createFromParcel(Parcel source) {
            return new Impuesto(source);
        }

        @Override
        public Impuesto[] newArray(int size) {
            return new Impuesto[size];
        }
    };
}
