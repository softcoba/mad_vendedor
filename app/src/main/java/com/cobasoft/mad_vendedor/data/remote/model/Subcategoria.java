/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cobasoft.mad_vendedor.data.remote.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.TypeConverters;

import com.cobasoft.mad_vendedor.data.local.converter.DateTypeConverter;
import com.google.gson.annotations.Expose;

import java.util.Date;


public class Subcategoria implements Parcelable {

    @ColumnInfo(name = "ID_SUBCATEGORIA")
    @Expose
    private Integer idSubcategoria;
    @Expose
    private String nombre;
    @Expose
    private String ubicacionFoto;
    @ColumnInfo(name = "FECHA_REGISTRO")
    @Expose
    @TypeConverters(DateTypeConverter.class)
    private Date fechaRegistro;
    @ColumnInfo(name = "ID_CATEGORIA")
    @Expose
    private Integer idCategoria;

    public Integer getIdSubcategoria() {
        return idSubcategoria;
    }

    public void setIdSubcategoria(Integer idSubcategoria) {
        this.idSubcategoria = idSubcategoria;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUbicacionFoto() {
        return ubicacionFoto;
    }

    public void setUbicacionFoto(String ubicacionFoto) {
        this.ubicacionFoto = ubicacionFoto;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    public Integer getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(Integer idCategoria) {
        this.idCategoria = idCategoria;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.idSubcategoria);
        dest.writeString(this.nombre);
        dest.writeString(this.ubicacionFoto);
        dest.writeLong(this.fechaRegistro != null ? this.fechaRegistro.getTime() : -1);
        dest.writeValue(this.idCategoria);
    }

    public Subcategoria() {
    }

    protected Subcategoria(Parcel in) {
        this.idSubcategoria = (Integer) in.readValue(Integer.class.getClassLoader());
        this.nombre = in.readString();
        this.ubicacionFoto = in.readString();
        long tmpFechaRegistro = in.readLong();
        this.fechaRegistro = tmpFechaRegistro == -1 ? null : new Date(tmpFechaRegistro);
        this.idCategoria = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Creator<Subcategoria> CREATOR = new Creator<Subcategoria>() {
        @Override
        public Subcategoria createFromParcel(Parcel source) {
            return new Subcategoria(source);
        }

        @Override
        public Subcategoria[] newArray(int size) {
            return new Subcategoria[size];
        }
    };
}
