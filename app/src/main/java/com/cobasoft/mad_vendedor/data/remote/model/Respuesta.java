/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cobasoft.mad_vendedor.data.remote.model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;

import com.google.gson.annotations.Expose;


public class Respuesta implements Parcelable {

    @ColumnInfo(name = "ID_RESPUESTA")
    @Expose
    private Integer idRespuesta;
    @Expose
    private String respuesta;
    @ColumnInfo(name = "ID_VENDEDOR")
    @Expose
    private Integer idVendedor;

    public Integer getIdRespuesta() {
        return idRespuesta;
    }

    public void setIdRespuesta(Integer idRespuesta) {
        this.idRespuesta = idRespuesta;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public Integer getIdVendedor() {
        return idVendedor;
    }

    public void setIdVendedor(Integer idVendedor) {
        this.idVendedor = idVendedor;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.idRespuesta);
        dest.writeString(this.respuesta);
        dest.writeValue(this.idVendedor);
    }

    public Respuesta() {
    }

    protected Respuesta(Parcel in) {
        this.idRespuesta = (Integer) in.readValue(Integer.class.getClassLoader());
        this.respuesta = in.readString();
        this.idVendedor = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Parcelable.Creator<Respuesta> CREATOR = new Parcelable.Creator<Respuesta>() {
        @Override
        public Respuesta createFromParcel(Parcel source) {
            return new Respuesta(source);
        }

        @Override
        public Respuesta[] newArray(int size) {
            return new Respuesta[size];
        }
    };
}
