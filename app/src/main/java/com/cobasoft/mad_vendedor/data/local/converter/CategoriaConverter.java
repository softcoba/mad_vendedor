package com.cobasoft.mad_vendedor.data.local.converter;

import androidx.room.TypeConverter;

import com.cobasoft.mad_vendedor.data.remote.model.Categoria;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class CategoriaConverter {

    @TypeConverter
    public Categoria fromString(String value) {
        Type type= new TypeToken<Categoria>() {
        }.getType();
        Categoria categoria = new Gson().fromJson(value, type);
        return categoria;
    }

    @TypeConverter
    public String fromList(Categoria categoria) {
        return new Gson().toJson(categoria);
    }
}
