package com.cobasoft.mad_vendedor.data.local.converter;

import androidx.room.TypeConverter;

import com.cobasoft.mad_vendedor.data.remote.model.Categoria;
import com.cobasoft.mad_vendedor.data.remote.model.FormaDePago;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class FormaDePagoConverter {

    @TypeConverter
    public FormaDePago fromString(String value) {
        Type type= new TypeToken<Categoria>() {
        }.getType();
        FormaDePago formaDePago = new Gson().fromJson(value, type);
        return formaDePago;
    }

    @TypeConverter
    public String fromList(FormaDePago formaDePago) {
        return new Gson().toJson(formaDePago);
    }
}
