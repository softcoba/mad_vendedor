/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cobasoft.mad_vendedor.data.remote.model;


import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.TypeConverters;

import com.cobasoft.mad_vendedor.data.local.converter.DateTypeConverter;
import com.google.gson.annotations.Expose;

import java.util.Date;


public class FotoProducto implements Parcelable {

    @ColumnInfo(name = "ID_FOTO_PRODUCTO")
    @Expose
    private Integer idFotoProducto;
    @Expose
    private String ubicacionFoto;
    @ColumnInfo(name = "FECHA_REGISTRO")
    @Expose
    @TypeConverters(DateTypeConverter.class)
    private Date fechaRegistro;

    public Integer getIdFotoProducto() {
        return idFotoProducto;
    }

    public void setIdFotoProducto(Integer idFotoProducto) {
        this.idFotoProducto = idFotoProducto;
    }

    public String getUbicacionFoto() {
        return ubicacionFoto;
    }

    public void setUbicacionFoto(String ubicacionFoto) {
        this.ubicacionFoto = ubicacionFoto;
    }

    public Date getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(Date fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.idFotoProducto);
        dest.writeString(this.ubicacionFoto);
        dest.writeLong(this.fechaRegistro != null ? this.fechaRegistro.getTime() : -1);
    }

    public FotoProducto() {
    }

    protected FotoProducto(Parcel in) {
        this.idFotoProducto = (Integer) in.readValue(Integer.class.getClassLoader());
        this.ubicacionFoto = in.readString();
        long tmpFechaRegistro = in.readLong();
        this.fechaRegistro = tmpFechaRegistro == -1 ? null : new Date(tmpFechaRegistro);
    }

    public static final Creator<FotoProducto> CREATOR = new Creator<FotoProducto>() {
        @Override
        public FotoProducto createFromParcel(Parcel source) {
            return new FotoProducto(source);
        }

        @Override
        public FotoProducto[] newArray(int size) {
            return new FotoProducto[size];
        }
    };
}
