package com.cobasoft.mad_vendedor.data.local.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import com.cobasoft.mad_vendedor.data.local.entity.RequestEntity;

@Dao
public interface SesionDao {

    @Query("SELECT * FROM RequestEntity where vendedor_correo = :correo")
    RequestEntity obtenerVendedorByCorreo(String correo);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertarBaseDeDatos(RequestEntity baseDeDatosEntity);

}
