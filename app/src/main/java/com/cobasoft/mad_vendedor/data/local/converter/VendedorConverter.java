package com.cobasoft.mad_vendedor.data.local.converter;

import androidx.room.TypeConverter;

import com.cobasoft.mad_vendedor.data.remote.model.Categoria;
import com.cobasoft.mad_vendedor.data.remote.model.Vendedor;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class VendedorConverter {

    @TypeConverter
    public Vendedor fromString(String value) {
        Type type= new TypeToken<Categoria>() {
        }.getType();
        Vendedor vendedor = new Gson().fromJson(value, type);
        return vendedor;
    }

    @TypeConverter
    public String fromList(Vendedor vendedor) {
        return new Gson().toJson(vendedor);
    }
}
