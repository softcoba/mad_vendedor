package com.cobasoft.mad_vendedor.data.local.converter;

import androidx.room.TypeConverter;

import com.cobasoft.mad_vendedor.data.remote.model.Categoria;
import com.cobasoft.mad_vendedor.data.remote.model.PedidoComprador;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class PedidoCompradorConverter {

    @TypeConverter
    public PedidoComprador fromString(String value) {
        Type type= new TypeToken<Categoria>() {
        }.getType();
        PedidoComprador pedidoComprador = new Gson().fromJson(value, type);
        return pedidoComprador;
    }

    @TypeConverter
    public String fromList(PedidoComprador pedidoComprador) {
        return new Gson().toJson(pedidoComprador);
    }
}
