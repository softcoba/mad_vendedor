package com.cobasoft.mad_vendedor.data.local.converter;

import androidx.room.TypeConverter;

import com.cobasoft.mad_vendedor.data.remote.model.Categoria;
import com.cobasoft.mad_vendedor.data.remote.model.MotorizadoPedido;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class MotorizadoPedidoConverter {

    @TypeConverter
    public MotorizadoPedido fromString(String value) {
        Type type= new TypeToken<Categoria>() {
        }.getType();
        MotorizadoPedido motorizadoPedido = new Gson().fromJson(value, type);
        return motorizadoPedido;
    }

    @TypeConverter
    public String fromList(MotorizadoPedido motorizadoPedido) {
        return new Gson().toJson(motorizadoPedido);
    }
}
