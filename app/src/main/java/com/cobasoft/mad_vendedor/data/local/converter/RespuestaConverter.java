package com.cobasoft.mad_vendedor.data.local.converter;

import androidx.room.TypeConverter;

import com.cobasoft.mad_vendedor.data.remote.model.Categoria;
import com.cobasoft.mad_vendedor.data.remote.model.Respuesta;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;

public class RespuestaConverter {

    @TypeConverter
    public Respuesta fromString(String value) {
        Type type= new TypeToken<Categoria>() {
        }.getType();
        Respuesta respuesta = new Gson().fromJson(value, type);
        return respuesta;
    }

    @TypeConverter
    public String fromList(Respuesta respuesta) {
        return new Gson().toJson(respuesta);
    }
}
